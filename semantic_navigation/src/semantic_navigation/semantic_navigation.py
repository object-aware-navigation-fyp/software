#!/usr/bin/env python
import rospy
import actionlib
import numpy as np
from visualization_msgs.msg import MarkerArray, Marker

from control_msgs.msg import PointHeadAction, PointHeadGoal
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from nav_msgs.srv import GetPlan
from geometry_msgs.msg import Vector3, PoseStamped, Quaternion
from tf.transformations import quaternion_from_euler

from math import pi as PI
from math import sin, cos, atan2


def create_marker(id, x,y,z):
    marker = Marker()
    marker.header.frame_id = 'map'
    marker.header.stamp = rospy.Time.now()
    marker.ns = 'foofoo'
    marker.id = id
    marker.type = 0
    marker.action = 0
    marker.lifetime = rospy.Duration(3)
    marker.pose.position.x = x
    marker.pose.position.y = y
    marker.pose.position.z = z + 0.25
    marker.pose.orientation.x = 0
    marker.pose.orientation.y = 0.71
    marker.pose.orientation.z = 0
    marker.pose.orientation.w = 0.71
    marker.scale.x = 0.25
    marker.scale.y = 0.05
    marker.scale.z = 0.05
    marker.color.a = 0.5
    marker.color.r = 0.0
    marker.color.g = 1.0
    marker.color.b = 0.0
    return marker


class SemanticNavigation:
    def __init__(self):
        # create move_base client
        self._move_base_client = actionlib.SimpleActionClient('/move_base', MoveBaseAction)
        self._move_base_client.wait_for_server()

        # marker pub
        self.marker_pub = rospy.Publisher('/semantic_navigation/plan', MarkerArray, queue_size=10, latch=1)

        # point head client
        self._point_head_client = actionlib.SimpleActionClient('/head_controller/point_head_action', PointHeadAction)
        self._point_head_client.wait_for_server()


    def move_to_marker(self, start, marker, target_dist, test_points=16):
        # goal pose
        goal = PoseStamped()
        goal.header.frame_id = marker.header.frame_id
        goal.header.stamp = marker.header.stamp
        goal.pose = marker.pose
        goal.pose.position.z = 0

        p1 = np.array([start.pose.position.x, start.pose.position.y])
        markers = MarkerArray()

        # generate points using parametric form of circle
        circle = []
        dists = []
        a = goal.pose.position.x
        b = goal.pose.position.y
        r = target_dist

        # compute angle between samples
        slice_angle = 2*PI/test_points
        current_angle = 0

        # create circle and array of distances between robot
        # and points along circle at slice_angle intervals.
        for i in range(test_points):
            x = a + r * cos(current_angle)
            y = b + r * sin(current_angle)

            # euclidean dist
            p2 = np.array([x,y])
            dist = np.linalg.norm(p1 - p2)

            # append point and distance
            circle.append((x, y))
            dists.append(dist)
            current_angle += slice_angle

            markers.markers.append(create_marker(i,x,y,0))

        # publish candidate points and sort distances
        self.marker_pub.publish(markers)
        dists_argsort = np.argsort(dists)

        for i in dists_argsort:
            x,y = circle[i]
            goal.pose.position.x = x
            goal.pose.position.y = y

            rospy.wait_for_service('/move_base/GlobalPlanner/make_plan')
            make_plan = rospy.ServiceProxy('/move_base/GlobalPlanner/make_plan', GetPlan)
            plan = make_plan(start, goal, 1.0)

            if len(plan.plan.poses) > 0:
                p2 = np.array([a,b])

                # only rotate in place if too close!!!
                dist = np.linalg.norm(p1 - p2)
                if dist < target_dist + 0.5:
                    goal.pose.position = start.pose.position

                # get the new rotation
                dist_x = p2[0] - p1[0]
                dist_y = p2[1] - p1[1]
                current_angle = atan2(dist_y, dist_x)
                (x, y, z, w) = quaternion_from_euler(0, 0, current_angle)
                goal.pose.orientation = Quaternion(x, y, z, w)

                mb_goal = MoveBaseGoal(goal)
                self._move_base_client.send_goal(MoveBaseGoal(goal))
                return self._move_base_client.wait_for_result, self._move_base_client.get_result

        return None, None

    def point_head_to_marker(self, marker, duration=0.3, max_velocity=1):
        ph_goal = PointHeadGoal()

        # set target point
        ph_goal.target.header.frame_id = marker.header.frame_id
        ph_goal.target.header.stamp = rospy.Time(0)
        ph_goal.target.point = marker.pose.position

        # set PointHead values
        ph_goal.pointing_axis = Vector3(1,0,0)
        ph_goal.pointing_frame = 'head_2_link'
        ph_goal.min_duration = rospy.Duration(duration)
        ph_goal.max_velocity = max_velocity

        self._point_head_client.send_goal(ph_goal)