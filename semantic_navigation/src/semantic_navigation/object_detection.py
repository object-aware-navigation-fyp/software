from abc import abstractmethod
import rospy
import numpy as np

from lasr_object_detection_yolo.srv import YoloDetection
from object_detection_mask_rcnn import MaskRCNN
from instance_segmentation_pytorch.srv import InstanceSegmentation as InstanceSegmentationSrv
from object_segmentation_pcl import SegmentationClient

from cv_bridge import CvBridge
from sensor_msgs.msg import PointCloud2


# get known objects list by rosparam
KNOWN_OBJECTS = []
if rospy.has_param('/semantic_map/known_objects'):
    KNOWN_OBJECTS = rospy.get_param('/semantic_map/known_objects')
    print 'Semantic Map: known objects are', KNOWN_OBJECTS


def pclmsg_to_pcl_cv2_imgmsg(pclmsg):
    # extract the xyz values from 32FC1
    pcl = np.fromstring(pclmsg.data, dtype=np.uint8)
    pcl = pcl.reshape(pclmsg.height, pclmsg.width, -1)

    # extract the rgb values from 32FC1
    frame = np.fromstring(pclmsg.data, dtype=np.uint8)
    frame = frame.reshape(pclmsg.height, pclmsg.width, 32)
    frame = frame[:,:,16:19].copy()

    # imgmsg
    bridge = CvBridge()
    imgmsg = bridge.cv2_to_imgmsg(frame, encoding='rgb8')

    return pcl, frame, imgmsg


def is_known_object(name):
    return len(KNOWN_OBJECTS) == 0 or name in KNOWN_OBJECTS


class YoloLccp:
    def __init__(self, namespace='yolo_detection'):
        self.sc = SegmentationClient()
        rospy.wait_for_service(namespace)
        self.yolo = rospy.ServiceProxy(namespace, YoloDetection)

    def detect(self, pclmsg, dataset='coco', confidence=0.7, nms=0.3, downsample=2):
        # yolo detection
        pcl, frame, imgmsg = pclmsg_to_pcl_cv2_imgmsg(pclmsg)
        predictions = self.yolo(imgmsg, dataset, confidence, nms)

        # results
        boxes = []
        clouds = []
        cuboids = []
        labels = []

        for detection in predictions.detected_objects:
            if is_known_object(detection.name):
                # extract detection from point cloud
                x,y,w,h = detection.xywh
                pcl_det = pcl[y:y+h:downsample, x:x+w:downsample, :]

                # we dont want detections on the edge
                if x < 10 or x + w > 630 or y < 10 or y > 470:
                    continue

                # create pcl
                pclmsg_out = PointCloud2()
                pclmsg_out.header       = pclmsg.header
                pclmsg_out.height       = pcl_det.shape[0]
                pclmsg_out.width        = pcl_det.shape[1]
                pclmsg_out.fields       = pclmsg.fields
                pclmsg_out.is_bigendian = pclmsg.is_bigendian
                pclmsg_out.point_step   = pclmsg.point_step
                pclmsg_out.row_step     = pcl_det.size/pcl_det.shape[0]
                pclmsg_out.is_dense     = pclmsg.is_dense
                pclmsg_out.data         = pcl_det.flatten().tostring()

                # lccp largest centroid
                res = self.sc.pipeline_lccp(pclmsg_out, 'map', use_sanity_criterion=1, voxel_resolution=0.02)
                # pclmsg_out = res.lccp_largest_centroid
                pclmsg_out = res.lccp_coloured_cloud
                min_point = res.min
                max_point = res.max

                # append results
                boxes.append([x,y,x+w,y+h])
                clouds.append(pclmsg_out)
                cuboids.append((min_point, max_point))
                labels.append(detection.name)
        
        return frame, pcl, boxes, clouds, cuboids, labels


class InstanceSegmentation:
    def __init__(self, model_path='/tiago_ws/src/software/instance_segmentation_pytorch/models/_default'):
        self.sc = SegmentationClient()
        self.model_path = model_path
        self.net_init()

    @abstractmethod
    def net_init(self):
        pass

    @abstractmethod
    def net_forward(self, frame):
        pass

    def detect(self, pclmsg, dataset='_default', confidence=0.7, mask_confidence=0.8, downsample=2):
        pcl, frame, imgmsg = pclmsg_to_pcl_cv2_imgmsg(pclmsg)
        pred_boxes, pred_labels, pred_labels_text, pred_scores, pred_masks = self.net_forward(frame)

        # results
        masks = []
        boxes = []
        clouds = []
        cuboids = []
        labels = []

        for i, label in enumerate(pred_labels_text):
            if pred_scores[i] > confidence and is_known_object(label):
                # downsample everything
                mask = pred_masks[i]
                frame_ds = frame[::downsample, ::downsample, :]
                pcl_ds = pcl[::downsample, ::downsample, :]
                mask_ds = mask[::downsample, ::downsample]

                # get indices of mask
                binary_mask = mask_ds > mask_confidence
                binary_mask = binary_mask.flatten()
                indices = np.argwhere(binary_mask).flatten()

                # extract segmented detection
                frame_out = np.take(frame_ds.reshape(frame_ds.shape[0] * frame_ds.shape[1], -1), indices, axis=0)
                pcl_out = np.take(pcl_ds.reshape(pcl_ds.shape[0] * pcl_ds.shape[1], -1), indices, axis=0)

                # create pcl
                pclmsg_out = PointCloud2()
                pclmsg_out.header       = pclmsg.header
                pclmsg_out.height       = pcl_out.shape[0]
                pclmsg_out.width        = 1
                pclmsg_out.fields       = pclmsg.fields
                pclmsg_out.is_bigendian = pclmsg.is_bigendian
                pclmsg_out.point_step   = pclmsg.point_step
                pclmsg_out.row_step     = pcl_out.shape[1]
                pclmsg_out.is_dense     = pclmsg.is_dense
                pclmsg_out.data         = pcl_out.flatten().tostring()

                # largest cluster
                res = self.sc.pipeline_cluster(pclmsg_out, 'map', 0.02, 0.02, 0.02, cluster_tolerance=0.04)
                pclmsg_out = res.points
                min_point = res.min
                max_point = res.max

                # append results
                masks.append(mask)
                boxes.append(pred_boxes[i])
                clouds.append(pclmsg_out)
                cuboids.append((min_point, max_point))
                labels.append(label)
        
        return frame, pcl, boxes, clouds, cuboids, labels, masks



class MaskRcnnCluster(InstanceSegmentation):
    def net_init(self):
        self.mask_rcnn = MaskRCNN(self.model_path)

    def net_forward(self, frame):
        return self.mask_rcnn.forward(self.model_path, frame)



class YolactCluster(InstanceSegmentation):
    def net_init(self):
        self.bridge = CvBridge()
        rospy.wait_for_service('/yolact/forward')
        self.yolact = rospy.ServiceProxy('/yolact/forward', InstanceSegmentationSrv)

    def net_forward(self, frame):
        imgmsg = self.bridge.cv2_to_imgmsg(frame, encoding='bgr8')
        predictions = self.yolact('', imgmsg)

        # extract information
        pred_count = predictions.count
        pred_masks = [self.bridge.imgmsg_to_cv2(mask).copy() for mask in predictions.masks]
        pred_boxes = np.array(predictions.boxes).reshape(max(pred_count, 1), -1)
        pred_labels = np.array(predictions.labels)
        pred_labels_text = predictions.labels_txt
        pred_scores = np.array(predictions.scores)

        # bbox segmentation to remove yolact "leakage"
        for i, mask in enumerate(pred_masks):
            x1,y1,x2,y2 = [int(v) for v in pred_boxes[i]]
            x1 = max(x1, 0)
            y1 = max(y1, 0)
            x2 = min(x2, frame.shape[1])
            y2 = min(y2, frame.shape[0])

            box_mask = np.zeros(mask.shape, dtype=mask.dtype)
            for x in range(x1,x2):
                for y in range(y1,y2):
                    box_mask[y,x] = 1
            pred_masks[i] *= box_mask

        return pred_boxes, pred_labels, pred_labels_text, pred_scores, pred_masks
