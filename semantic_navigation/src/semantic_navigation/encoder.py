"""
Appearance Metric Encoding

This is implemented using the "fullbodynet" metric learned through triplet loss.
Feel free to replace "fullbodynet" with your appearance metric encoder of choice.

fullbodynet: https://gitlab.com/object-aware-navigation-fyp/fullbodynet
"""

from fullbodynet.encoder import Encoder
fullbodynet_encoder = Encoder()

def get_encoder():
    return fullbodynet_encoder.encode