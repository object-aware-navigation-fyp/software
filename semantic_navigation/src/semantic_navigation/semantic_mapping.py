#!/usr/bin/env python
import rospy
import numpy as np
import sys
from math import ceil, floor
from copy import deepcopy

from sensor_msgs.msg import Image, PointCloud2
from visualization_msgs.msg import Marker, MarkerArray

from multiple_object_tracking import AB3DMOT
from object_detection import *

from PIL import Image as PIL_Image

import tf2_ros
from tf2_geometry_msgs.tf2_geometry_msgs import do_transform_point
from std_msgs.msg import Header
from geometry_msgs.msg import PointStamped, Point
from sensor_msgs.msg import CameraInfo

from glasbey import PALETTE_128
COLOURS = np.array(PALETTE_128)/255.0


def get_camera_info(namespace):
    # get camera info from ROS topic
    try:
        cam_info = rospy.wait_for_message(namespace + '/camera_info', CameraInfo, 5.0)

    # load the pickled xtion default settings if we timeout
    except rospy.ROSException:
        import pickle
        import rospkg
        PKG_ROOT = rospkg.RosPack().get_path('semantic_navigation')
        with open(PKG_ROOT + '/config/camera_info_xtion.pkl') as f:
            cam_info = pickle.load(f)

    return cam_info



def nms_2d(tracks, threshold=0.1):
    """
    2D Non-Maximum Supression (NMS)

    Implements NMS to remove duplicate
    detections from our tracker.

    Reference: https://www.pyimagesearch.com/2015/02/16/faster-non-maximum-suppression-python/
    """
    x1 = []
    y1 = []
    x2 = []
    y2 = []
    area = []

    for track in tracks:
        # x,y
        x = track[3]
        y = track[4]

        # width, height
        w = track[0]
        h = track[1]
        w_half = w/2.0
        h_half = h/2.0

        # get x1,y1,x2,y2 and area of track
        x1.append(x - w_half)
        x2.append(x + w_half)
        y1.append(y - h_half)
        y2.append(y + h_half)
        area.append(w * h)

    # numpify
    x1 = np.array(x1)
    y1 = np.array(y1)
    x2 = np.array(x2)
    y2 = np.array(y2)
    area = np.array(area)

    # sort by area (ascending)
    kept_indices = []
    removed_indices = []
    indices = np.argsort(area)

    while len(indices) > 0:
        # choose the largest track
        i = indices[-1]
        kept_indices.append(i)

        # vectorised bbs
        bb_x1 = np.maximum(x1[i], x1[indices[:-1]])
        bb_y1 = np.maximum(y1[i], y1[indices[:-1]])
        bb_x2 = np.minimum(x2[i], x2[indices[:-1]])
        bb_y2 = np.minimum(y2[i], y2[indices[:-1]])
        bb_w = np.maximum(0, bb_x2 - bb_x1)
        bb_h = np.maximum(0, bb_y2 - bb_y1)

        # vectorised width and height of bbs
        overlap = (bb_w * bb_h) / area[indices[:-1]]

        # indices to remove
        to_delete = np.where(overlap > threshold)
        removed_indices += indices[to_delete].tolist()

        # remove current index and any large overlaps
        indices = np.delete(indices, -1)
        indices = np.delete(indices, to_delete)

    return kept_indices, removed_indices



class SemanticMapping:
    def __init__(self, args):
        # set constants
        self.max_range = args.max_range
        self.min_unseen = args.min_unseen
        self.margin_unseen = args.margin_unseen/2.0 # halved accounting for each side
        self.confidence_threshold = args.confidence
        self.dataset = args.dataset
        self.map_nms_threshold = args.map_nms
        self.occl_side = args.occlusion_side
        self.occl_dist = args.occlusion_dist

        # transform stuff
        self.tf_buffer = tf2_ros.Buffer()
        tf_listener = tf2_ros.TransformListener(self.tf_buffer)

        # get camera info and extract projection matrix
        self.cam_info = get_camera_info(args.camera_ns)
        self.projection_matrix = np.array(self.cam_info.P).reshape(3,4)

        # create object detector and tracker
        if args.use_yolo:
            self.detector = YoloLccp()
        elif args.use_yolact:
            self.detector = YolactCluster()
        else:
            self.detector = MaskRcnnCluster()
        self.sort = AB3DMOT(max_age=3000) # approx 100 seconds

        # create an encoder object if file path is supplied
        self.encoder = None
        if args.use_encoder:
            try:
                from encoder import get_encoder
                self.encoder = get_encoder()
            except:
                print 'WARNING: get_encoder() not defined in encoder.py, no appearance metrics will be used.'

        # create publisher and subscribe indefinitely
        self.marker_pub = rospy.Publisher('/semantic_map/markers', MarkerArray, queue_size=10)
        self.marker_text_pub = rospy.Publisher('/semantic_map/marker_labels', MarkerArray, queue_size=10)
        self.pcl_sub = rospy.Subscriber(args.camera_ns + '/points', PointCloud2, self.callback, queue_size=1)


    def callback(self, pclmsg):
        trans = self.tf_buffer.lookup_transform(pclmsg.header.frame_id, 'map', pclmsg.header.stamp, rospy.Duration(2.0))
        frame, pcl, boxes, clouds, cuboids, labels = self.detector.detect(pclmsg, dataset=self.dataset, confidence=self.confidence_threshold)[:6]

        # preprocess data for tracker input
        detections = []
        info = []
        images = []
        bboxes = []
        for i, label in enumerate(labels):
            x1,y1,x2,y2 = [int(v) for v in boxes[i]]
            pw = (cuboids[i][1].x - cuboids[i][0].x)
            ph = (cuboids[i][1].y - cuboids[i][0].y)
            pl = (cuboids[i][1].z - cuboids[i][0].z)
            px = cuboids[i][0].x + pw/2
            py = cuboids[i][0].y + ph/2
            pz = cuboids[i][0].z + pl/2
            detection = [pw, ph, pl, px, py, pz, 0]

            # transform to camera frame
            header = Header(frame_id='map', stamp=pclmsg.header.stamp)
            pointstamped = PointStamped(header=header, point=Point(px,py,pz))
            pointstamped = do_transform_point(pointstamped, trans)

            # skip detection if out of range or NaN encountered
            if pointstamped.point.z >= self.max_range or not np.all(np.isfinite(detection)):
                continue

            # store detection and info
            detections.append(detection)
            info.append([x1,y1,x2,y2,label])

            # get images if encoder is set
            if self.encoder is not None:
                img = frame[y1:y2, x1:x2, ::-1]
                image = PIL_Image.fromarray(img)
                images.append(image)


        # compute deep features
        features = None
        if self.encoder is not None:
            features = self.encoder(images)

        # update tracker
        dets_all = {'dets':np.array(detections), 'info':np.array(info), 'features':features}
        results = self.sort.update(dets_all)

        # non-maximum supression
        kept_indices, removed_indices = nms_2d(results, self.map_nms_threshold)
        results_temp = []

        # choose kept_indices and remove overlaps
        for i in kept_indices:
            results_temp.append(results[i])
        for i in removed_indices:
            self.sort.delete(results[i][7])
        results = results_temp

        # create marker arrays
        marker_array = MarkerArray()
        marker_text_array = MarkerArray()

        # process tracked objects
        for result in results:
            sx,sy,sz = result[0:3]
            px,py,pz = result[3:6]
            x1,y1,x2,y2 = [int(v) for v in result[9:13]]
            time_since_update = result[8]
            identity = result[7]
            label = result[13]

            # transform to camera frame
            header = Header(frame_id='map', stamp=pclmsg.header.stamp)
            pointstamped = PointStamped(header=header, point=Point(px,py,pz))
            pointstamped = do_transform_point(pointstamped, trans)

            # project onto camera
            # ref: http://docs.ros.org/en/melodic/api/sensor_msgs/html/msg/CameraInfo.html
            px_tf, py_tf, pz_tf = pointstamped.point.x, pointstamped.point.y, pointstamped.point.z
            point = np.array([px_tf, py_tf, pz_tf, 1])
            u,v,w = self.projection_matrix.dot(point)
            x = int(u/w)
            y = int(v/w)

            # point is "inside" if
            # it is not behind the camera/it is bounded by the 2D frame
            is_inside = (pz_tf >= 0 and
                         x >= self.cam_info.width * self.margin_unseen and
                         x <= self.cam_info.width * (1.0 - self.margin_unseen) and
                         y >= self.cam_info.height * self.margin_unseen and
                         y <= self.cam_info.height * (1.0 - self.margin_unseen))

            # check for occlusion if point is inside frame
            is_occluded = False
            if is_inside and self.occl_side > 0:
                occl_ceil = int(ceil(self.occl_side/2.0))
                occl_floor = int(floor(self.occl_side/2.0))
                occlusion_sample = pcl[y-occl_floor : y+occl_ceil, x-occl_floor : x+occl_ceil]
                occlusion_sample_z = occlusion_sample.copy().view(np.float32)[:,:,2]
                occlusion_sample_z = occlusion_sample_z[np.isfinite(occlusion_sample_z)] # filter NaNs

                # get the median distance of the sample
                if occlusion_sample_z.size == 0:
                    median_z = float('nan')
                else:
                    median_z = np.median(occlusion_sample_z)

                # object is occluded if median dist is NaN or
                # occl_dist metres closer than expected
                if np.isfinite(median_z) and pz_tf - median_z > self.occl_dist:
                    is_occluded = True

            # delete undetected, non-occluded points "inside" and within range
            # size should also be greater than 0 to be considered
            if (not is_occluded and
                    (pz_tf <= self.max_range and is_inside and time_since_update > self.min_unseen) or
                    (sx <= 0 and sy <= 0 and sz <= 0)):
                self.sort.delete(identity)
                continue

            # create markers
            marker = self.create_marker(label, identity, px, py, pz, sx, sy, sz)
            marker_text = deepcopy(marker)
            marker_text.type = 9
            marker_text.scale.z = 0.1
            marker_text.color.a = 1.0

            marker_array.markers.append(marker)
            marker_text_array.markers.append(marker_text)

        # publish markers
        self.marker_pub.publish(marker_array)
        self.marker_text_pub.publish(marker_text_array)


    def create_marker(self, label, identity, px, py, pz, sx, sy, sz):
        marker = Marker()
        marker.header.frame_id = 'map'
        marker.header.stamp = rospy.Time.now()
        marker.ns = label
        marker.id = identity
        marker.type = 1
        marker.action = 0
        marker.lifetime = rospy.Duration(0.5)
        marker.pose.position.x = px
        marker.pose.position.y = py
        marker.pose.position.z = pz
        marker.pose.orientation.x = 0
        marker.pose.orientation.y = 0
        marker.pose.orientation.z = 0
        marker.pose.orientation.w = 1
        marker.scale.x = sx
        marker.scale.y = sy
        marker.scale.z = sz

        colour = COLOURS[int(identity%len(COLOURS))]
        marker.color.a = 0.5
        marker.color.r = colour[0]
        marker.color.g = colour[1]
        marker.color.b = colour[2]

        marker.text = 'id: {}\n{}'.format(marker.id, marker.ns)

        return marker


if __name__ == '__main__':
    import argparse

    # parse arguments
    parser = argparse.ArgumentParser('Run the semantic map publisher.')
    parser.add_argument('--use_yolo', dest='use_yolo', type=int, default='0', help='If set, use YOLOv3 + LCCP for detection. Defaults to Mask R-CNN otherwise.')
    parser.add_argument('--use_yolact', dest='use_yolact', type=int, default='0', help='If set, use YOLACT for detection (experimental). Defaults to Mask R-CNN otherwise.')
    parser.add_argument('--use_encoder', dest='use_encoder', type=int, default='0', help='If set, use Encoder class for computing deep appearance metrics from PIL images.')
    parser.add_argument('--max_range', dest='max_range', type=float, default=3.5, help='Max range in metres for detecting/deleting tracked objects.')
    parser.add_argument('--min_unseen', dest='min_unseen', type=int, default=10, help='Min frames an object has not been updated in before it can be deleted.')
    parser.add_argument('--margin_unseen', dest='margin_unseen', type=float, default=0.1, help='Margin around image where unseen objects are not deleted.')
    parser.add_argument('--confidence', dest='confidence', type=float, default=0.7, help='Detection confidence threshold.')
    parser.add_argument('--dataset', dest='dataset', type=str, default='_default', help='Dataset weights to load for object detection.')
    parser.add_argument('--map_nms', dest='map_nms', type=float, default=0.1, help='2D Non-Maximum Supression threshold for mapping. Set to a value >1 to disable.')
    parser.add_argument('--occlusion_side', dest='occlusion_side', type=int, default=7, help='Side of sample square (px) to check for occlusion. Set to 0 to disable.')
    parser.add_argument('--occlusion_dist', dest='occlusion_dist', type=float, default=1.0, help='Distance between object and point cloud coordinates that signals an occlusion.')
    parser.add_argument('--camera_ns', dest='camera_ns', default='/xtion/depth_registered', help='Namespace for CameraInfo and PointCloud2 topics e.g. "/xtion/depth_registered".')
    args, extras = parser.parse_known_args()

    # ros stuff
    rospy.init_node('test_sort')

    # start mapping
    semmap = SemanticMapping(args)

    while not rospy.is_shutdown():
        rospy.spin()
