# Semantic Mapping and Navigation
## Semantic Mapping

    roslaunch semantic_navigation semantic_mapping.launch

optional arguments (also present in the launch file):

    usage: Run the semantic map publisher. [-h] [--use_yolo USE_YOLO]
                                        [--use_yolact USE_YOLACT]
                                        [--use_encoder USE_ENCODER]
                                        [--max_range MAX_RANGE]
                                        [--min_unseen MIN_UNSEEN]
                                        [--margin_unseen MARGIN_UNSEEN]
                                        [--confidence CONFIDENCE]
                                        [--dataset DATASET] [--map_nms MAP_NMS]
                                        [--occlusion_side OCCLUSION_SIDE]
                                        [--occlusion_dist OCCLUSION_DIST]
                                        [--camera_ns CAMERA_NS]

    optional arguments:
    -h, --help            show this help message and exit
    --use_yolo USE_YOLO   If set, use YOLOv3 + LCCP for detection. Defaults to
                            Mask R-CNN otherwise.
    --use_yolact USE_YOLACT
                            If set, use YOLACT for detection (experimental).
                            Defaults to Mask R-CNN otherwise.
    --use_encoder USE_ENCODER
                            If set, use Encoder class for computing deep
                            appearance metrics from PIL images.
    --max_range MAX_RANGE
                            Max range in metres for detecting/deleting tracked
                            objects.
    --min_unseen MIN_UNSEEN
                            Min frames an object has not been updated in before it
                            can be deleted.
    --margin_unseen MARGIN_UNSEEN
                            Margin around image where unseen objects are not
                            deleted.
    --confidence CONFIDENCE
                            Detection confidence threshold.
    --dataset DATASET     Dataset weights to load for object detection.
    --map_nms MAP_NMS     2D Non-Maximum Supression threshold for mapping. Set
                            to a value >1 to disable.
    --occlusion_side OCCLUSION_SIDE
                            Side of sample square (px) to check for occlusion. Set
                            to 0 to disable.
    --occlusion_dist OCCLUSION_DIST
                            Distance between object and point cloud coordinates
                            that signals an occlusion.
    --camera_ns CAMERA_NS
                            Namespace for CameraInfo and PointCloud2 topics e.g.
                            "/xtion/depth_registered".



## Semantic Navigation
Within a python script:

    from semantic_navigation import SemanticNavigation
    navigator = SemanticNavigation()



### Move to Marker

    navigator.move_to_marker(start, marker, target_dist, test_points)

Input:

| Name        | Type                      | Description                                  |
|-------------|---------------------------|----------------------------------------------|
| start       | geometry_msgs/PoseStamped | start pose for navigation                    |
| marker      | visualization_msgs/Marker | object to move towards                       |
| target_dist | float                     | target distance from object                  |
| test_points | int                       | number of candidate approaches around object |



### Look at Marker

    navigator.point_head_to_marker(marker, duration, max_velocity)

Input:

| Name         | Type                      | Description                    |
|--------------|---------------------------|--------------------------------|
| marker       | visualization_msgs/Marker | object to look at              |
| duration     | float                     | minimum time to point head     |
| max_velocity | float                     | maximum velocity to point head |



## Scripts
Go to an object on the semantic map:

    rosrun semantic_navigation goto_object.py <object name>

Count instances of an class on the semantic map:

    rosrun semantic_navigation count_object <object name>

Follow an object on the semantic map:

    rosrun semantic_navigation count_object <object ID>



## Using an Encoder
Semantic Mapping supports the use of encoders to perform cosine distance matching between tracked objects and incoming detections, inspired by Deep SORT and FaceNet.

    roslaunch semantic_navigation semantic_mapping.launch use_encoder:=1

### Full Body Net
It is recommended to use the `fullbodynet` encoder included with the project deliverables, available at: https://gitlab.com/object-aware-navigation-fyp/fullbodynet.

Clone the repository into `/src/semantic_navigation` to use the encoder with `semantic_mapping`.

### Custom Encoders
To implement a custom encoder, create a python class with the following function definition and make changes to `encoder.py` accordingly. 

    class Encoder:
        encode(self, images, batch_size=8):
            # pre-processing and forward pass goes here.
            return np.array(encodings)



## Credits
### Deep SORT
https://arxiv.org/abs/1703.07402

### FaceNet
https://arxiv.org/abs/1503.03832