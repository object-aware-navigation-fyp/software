#!/usr/bin/env python

import rospy
import sys
import numpy as np

from visualization_msgs.msg import MarkerArray
from geometry_msgs.msg import PoseStamped, PoseWithCovarianceStamped

from threading import Thread, Lock
from copy import deepcopy

from semantic_navigation.semantic_navigation import SemanticNavigation

class Follower:
    def __init__(self):
        self.navigator = SemanticNavigation()
        self.is_following = 1
        self.markers = MarkerArray()

        # locks
        self.markers_lock = Lock()
        self.pose_lock = Lock()

        self._marker_sub = rospy.Subscriber('/semantic_map/markers', MarkerArray, self.set_markers)
        # self._pose_sub = rospy.Subscriber('/amcl_pose', PoseWithCovarianceStamped, self.set_pose)
        self._pose_sub = rospy.Subscriber('/slam_karto_pose', PoseWithCovarianceStamped, self.set_pose)


    def follow(self, id):
        rate = rospy.Rate(30) # 30hz
        time_last_move = rospy.Time.now()

        while self.is_following and not rospy.is_shutdown():
            markers = self.get_markers() # thread-safe

            # search for target
            for marker in markers:
                if marker.id == id:
                    # look at target
                    self.navigator.point_head_to_marker(marker)

                    # move to target
                    if rospy.Time.now() - time_last_move > rospy.Duration(2.0):
                        move_base_thread = Thread(target=self.move_to_target, args=(marker,))
                        move_base_thread.start()
                        time_last_move = rospy.Time.now()

            rate.sleep()


    def move_to_target(self, target):
        # start pose
        pose = self.get_pose()
        start = PoseStamped()
        start.header = pose.header
        start.pose = pose.pose.pose

        self.navigator.move_to_marker(start, target, 1.0)


    def set_markers(self, marker_array):
        self.markers_lock.acquire()
        self.markers = marker_array
        self.markers_lock.release()


    def set_pose(self, pose):
        self.pose_lock.acquire()
        self.pose = pose
        self.pose_lock.release()


    def get_markers(self):
        self.markers_lock.acquire()
        markers = deepcopy(self.markers.markers)
        self.markers_lock.release()
        return markers


    def get_pose(self):
        self.pose_lock.acquire()
        pose = deepcopy(self.pose)
        self.pose_lock.release()
        return pose



if __name__ == '__main__':
    rospy.init_node('follow_object')
    follower = Follower()

    if not len(sys.argv) == 2:
        print 'usage: python follow_object.py <object id>'
        sys.exit(0)

    follower.follow(int(sys.argv[1]))
    rospy.spin()