#!/usr/bin/env python

import rospy
import sys
from visualization_msgs.msg import MarkerArray


if __name__ == '__main__':
    rospy.init_node('count_object')

    if not len(sys.argv) == 2:
        print 'usage: python count.py <object name>'
        sys.exit(0)

    # count objects
    count = 0
    markers = rospy.wait_for_message('/semantic_map/markers', MarkerArray)
    for marker in markers.markers:
        if marker.ns == sys.argv[1]:
            count += 1

    rospy.loginfo('Found {} of {}'.format(count, sys.argv[1]))