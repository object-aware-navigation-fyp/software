#!/usr/bin/env python
import rospy
import numpy as np
import cv2
from cv_bridge import CvBridge, CvBridgeError
from nav_msgs.msg import OccupancyGrid
from sensor_msgs.msg import Image
from visualization_msgs.msg import MarkerArray
from nav_msgs.msg import Path
from copy import deepcopy
from threading import Lock

class SemanticMapDrawer:
    def __init__(self):
        # locks
        self.path_lock = Lock()
        self.map_lock = Lock()

        # set current plan and map
        self.latest_plan = Path()
        self.set_map(rospy.wait_for_message('/map', OccupancyGrid))

        # cv bridge
        self.bridge = CvBridge()

        # pub/sub
        self._img_pub = rospy.Publisher('/semantic_map/image', Image, queue_size=10)
        self._grid_pub = rospy.Publisher('/semantic_map/map', OccupancyGrid, queue_size=10)
        self._marker_sub = rospy.Subscriber('/semantic_map/markers', MarkerArray, self.marker_array_callback, queue_size=1)
        self._path_sub = rospy.Subscriber('/move_base/current_plan', Path, self.set_path, queue_size=1)
        self._map_sub = rospy.Subscriber('/map', OccupancyGrid, self.set_map, queue_size=1)


    def marker_array_callback(self, marker_array):
        # get current map
        mapmsg, img, grid = self.get_map()
        if len(marker_array.markers):
            time_gap = rospy.Time.now() - marker_array.markers[0].header.stamp

        for marker in marker_array.markers:
            # x,y,w,h accounting for map offset
            x = marker.pose.position.x - self.mapmsg.info.origin.position.x
            y = marker.pose.position.y - self.mapmsg.info.origin.position.y
            w = marker.scale.x
            h = marker.scale.y

            # get bounding box in map coordinates
            x1 = x - w/2.0
            y1 = y - h/2.0
            x2 = x + w/2.0
            y2 = y + h/2.0

            # convert to pixel coordinates
            x1 = int(x1/self.mapmsg.info.resolution)
            y1 = int(y1/self.mapmsg.info.resolution)
            x2 = int(x2/self.mapmsg.info.resolution)
            y2 = int(y2/self.mapmsg.info.resolution)

            # draw on map
            colour = [marker.color.b, marker.color.g, marker.color.r] # BGR colour
            colour = [int(c * 255) for c in colour]
            cv2.rectangle(img, (x1,y1), (x2,y2), colour, -1)
            cv2.rectangle(grid, (x1,y1), (x2,y2), 200, -1)

        # get latest plan
        current_plan = self.get_path()

        # draw robot base
        if len(current_plan.poses):
            x = current_plan.poses[0].pose.position.x - mapmsg.info.origin.position.x
            y = current_plan.poses[0].pose.position.y - mapmsg.info.origin.position.y
            x = int(x/mapmsg.info.resolution)
            y = int(y/mapmsg.info.resolution)
            r = int(0.27/mapmsg.info.resolution)
            cv2.circle(img, (x,y), r, (221,221,221), -1)

        # draw path on map
        if rospy.Time.now() - current_plan.header.stamp < rospy.Duration(5.0):
            for posestamped in current_plan.poses:
                x = posestamped.pose.position.x - mapmsg.info.origin.position.x
                y = posestamped.pose.position.y - mapmsg.info.origin.position.y
                x = int(x/mapmsg.info.resolution)
                y = int(y/mapmsg.info.resolution)
                cv2.circle(img, (x,y), 1, (0,0,255), -1)

        # image is upside down
        img = np.flip(img, 0)

        # publish image
        try:
            imgmsg = self.bridge.cv2_to_imgmsg(img, encoding='bgr8')
            self._img_pub.publish(imgmsg)
        except CvBridgeError as e:
            print e

        # publish occupancy grid
        msg = deepcopy(mapmsg)
        msg.data = grid.astype(np.int8).flatten().tolist()
        self._grid_pub.publish(msg)


    def set_path(self, path):
        self.path_lock.acquire()
        self.latest_plan = path
        self.path_lock.release()


    def set_map(self, mapmsg):
        self.map_lock.acquire()

        # store current mapmsg
        self.mapmsg = mapmsg

        # convert map data to np array
        self.map_np = np.array(mapmsg.data, dtype=np.uint8)
        self.map_np = self.map_np.reshape((mapmsg.info.height, mapmsg.info.width))

        # create img of map
        self.map_img = 255 - self.map_np.copy()
        self.map_img = np.expand_dims(self.map_img, 2)
        self.map_img = np.repeat(self.map_img, 3, -1)

        self.map_lock.release()


    def get_path(self):
        self.path_lock.acquire()
        current_plan = deepcopy(self.latest_plan)
        self.path_lock.release()
        return current_plan


    def get_map(self):
        self.map_lock.acquire()

        # copy everything
        mapmsg = deepcopy(self.mapmsg)
        img = self.map_img.copy()
        grid = self.map_np.copy()

        self.map_lock.release()
        return mapmsg, img, grid




if __name__ == '__main__':
    rospy.init_node('draw_semantic_map')
    drawer = SemanticMapDrawer()
    rospy.spin()