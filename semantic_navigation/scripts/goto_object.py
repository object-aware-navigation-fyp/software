#!/usr/bin/env python

import rospy
import sys
import numpy as np
from threading import Thread
from visualization_msgs.msg import MarkerArray
from geometry_msgs.msg import PoseStamped, PoseWithCovarianceStamped
from semantic_navigation.semantic_navigation import SemanticNavigation
from copy import deepcopy


if __name__ == '__main__':
    rospy.init_node('goto_object')
    navigator = SemanticNavigation()

    if not len(sys.argv) == 2:
        print 'usage: python goto_object.py <object name>'
        sys.exit(0)

    # search for target
    target = None
    markers = rospy.wait_for_message('/semantic_map/markers', MarkerArray)
    for marker in markers.markers:
        if marker.ns == sys.argv[1]:
            target = marker

    if target is None:
        rospy.loginfo('object not found.')
    else:
        # get start pose
        pose = rospy.wait_for_message('/slam_karto_pose', PoseWithCovarianceStamped)
        start = PoseStamped()
        start.header = pose.header
        start.pose = pose.pose.pose

        # go to the object
        wait_for_result, get_result = navigator.move_to_marker(start, deepcopy(target), 1.5)
        if wait_for_result is not None:
            while not wait_for_result(rospy.Duration(0.1)):
                navigator.point_head_to_marker(deepcopy(target))
