# 3D Multiple Object Tracking
python catkin object for AB3DMOT 3D Multiple Object Tracking (MOT).

## API
Inputs:

| Information  | Variables              | Description     |
|--------------|------------------------|-----------------|
| size         | size_x, size_y, size_z | 3D size         |
| position     | x, y, z                | 3D position     |
| rotation     | theta                  | Z-axis rotation |
| bounding box | x1, y1, x2, y2         | 2D bounding box |
| label        | label                  | class label     |
| features     | features               | feature vector  |

Outputs:

| Information       | Variables              | Description         |
|-------------------|------------------------|---------------------|
| size              | size_x, size_y, size_z | 3D size             |
| position          | x, y, z                | 3D position         |
| rotation          | theta                  | Z-axis rotation     |
| ID                | identity               | tracking ID         |
| time since update | time_since_update      | frames since update |
| bounding box      | x1, y1, x2, y2         | 2D bounding box     |
| label             | label                  | class label         |
| features          | features               | feature vector      |

Please refer to the "Quick Start" guide for formatting of input/output.

## Quick Start
From within a python script:

    from multiple_object_tracking import AB3DMOT
    ab3dmot = AB3DMOT()

Performing an update:

    detections = (size_x, size_y, size_z, x, y, z, 0)
    info = (x1, y1, x2, y2, label)
    features = None
    # features = some_feature_encoder([image])

    dets_all = {'dets':np.array(detections), 'info':np.array(info), 'features':np.array(features)}
    results = ab3dmot.update(dets_all)
    for result in results:
        size_x, size_y, size_z, x, y, z, theta, identity, time_since_update, x1, y1, x2, y2, label = result

## Credits
### AB3DMOT
https://github.com/xinshuoweng/AB3DMOT

### KITTI Object Tracking Evaluation
http://www.cvlibs.net/datasets/kitti/eval_tracking.php