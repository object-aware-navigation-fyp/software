# Object Aware Navigation for Mobile Robots with 3D MOT

## Installation
    pip install torch torchvision future tqdm scipy filterpy
    apt-get update && apt-get install ros-kinetic-bfl

    git clone --recursive https://gitlab.com/object-aware-navigation-fyp/software.git
    catkin build
Please refer to individual packages for setup of Deep Neural Network (DNN) weights.


## Packages
### Deliverables
[Instance Segmentation PyTorch](/instance_segmentation_pytorch)

[Object Segmentation PCL](/object_segmentation_pcl)

[Multiple Object Tracking (MOT)](/multiple_object_tracking)

[Semantic Mapping/Navigation](/semantic_navigation)

[Object Aware Move Base](/object_aware_navigation/object_aware_move_base)

[Object Aware Handlers](/object_aware_handlers)

[Search and Fetch](/search_and_fetch/search_and_fetch)


### Core
[AB3DMOT]()

[Full Body Net]()

[ROS Navigation Stack](/object_aware_navigation)

[Frontier Exploration](/frontier_exploration)


## Experiments
### Quick Start
#### Go To Object
    roslaunch semantic_navigation semantic_mapping.launch
    rosrun semantic_navigation goto_object <object name>

#### Count Objects
    roslaunch semantic_navigation semantic_mapping.launch
    rosrun semantic_navigation count_object <object name>

#### Follow Object
    roslaunch semantic_navigation semantic_mapping.launch
    rosrun semantic_navigation count_object <object ID>

#### Navigate Through Person/Small Object
    roslaunch semantic_navigation semantic_mapping.launch
    roslaunch object_aware_move_base object_aware_move_base.launch

optionally:

    rosrun object_aware_move_base reroute_move_base_goals.py
    rosrun object_aware_handlers <custom handler node>

#### Search and Fetch
    roslaunch search_and_fetch search_and_fetch.launch
    rosrun search_and_fetch search_and_fetch.py


## Credits
### PyTorch-YOLOv3
https://github.com/eriklindernoren/PyTorch-YOLOv3

### torchvision
https://pytorch.org/vision/stable/index.html

### Point Cloud Library
https://pointclouds.org/

### AB3DMOT
https://github.com/xinshuoweng/AB3DMOT

### ROS Navigation Stack
http://wiki.ros.org/navigation

### Frontier Exploration (ROS)
http://wiki.ros.org/frontier_exploration