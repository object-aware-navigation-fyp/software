# Object Aware Handlers
Implementations of two simple "Object Aware Handlers" for use with `object_aware_move_base`.

## Handle Persons
Asks persons to move aside and waits for a path to be clear.

    rosrun object_aware_handlers person_handler.py

## Handle Small Objects
Navigates to and picks up small objects obstructing the navigation path.

    rosrun object_aware_handlers simple_pick
    rosrun object_aware_handlers small_object_handler.py