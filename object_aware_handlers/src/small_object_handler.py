#!/usr/bin/env python
import rospy
import actionlib
import numpy as np

from visualization_msgs.msg import MarkerArray
from geometry_msgs.msg import PoseStamped, PoseWithCovarianceStamped
from visualization_msgs.msg import MarkerArray

from object_aware_move_base.msg import HandleObjectAction, HandleObjectResult
from semantic_navigation.semantic_navigation import SemanticNavigation
from object_aware_handlers.srv import Pick

class HandleServer:
    def __init__(self):
        self.timeout = rospy.Duration(5.0)
        self.navigator = SemanticNavigation()

        rospy.wait_for_service('pick_known_object')
        self.pick = rospy.ServiceProxy('pick_known_object', Pick)

        self._as = actionlib.SimpleActionServer('/object_aware_move_base/handlers/coffee', HandleObjectAction, execute_cb=self.execute_cb, auto_start=False)
        self._as.start()


    def execute_cb(self, goal):
        rospy.loginfo('Small object handler: handling at x:{:.2f} y:{:.2f} z:{:.2f}'.format(
            goal.marker.pose.position.x,
            goal.marker.pose.position.y,
            goal.marker.pose.position.z
        ))

        # get start pose
        pose = rospy.wait_for_message('/slam_karto_pose', PoseWithCovarianceStamped)
        start = PoseStamped()
        start.header = pose.header
        start.pose = pose.pose.pose

        # go to the object
        wait_for_result, get_result = self.navigator.move_to_marker(start, goal.marker, 0.5)
        wait_for_result()
        self.navigator.point_head_to_marker(goal.marker)

        rospy.sleep(1.0)
        start_time = rospy.Time.now()
        target = None

        while rospy.Time.now() - start_time < self.timeout:
            markers = rospy.wait_for_message('/semantic_map/markers', MarkerArray)
            for marker in markers.markers:
                if marker.ns == 'coffee':
                    target = marker

            if target is not None:
                # try to pick the object
                result = self.pick(marker)
                print result
                if result.error:
                    break

        # success if count is non-zero
        if result.error:
            rospy.loginfo('Small object handler: made a clearing.')
            self._as.set_succeeded(HandleObjectResult(1))
        else:
            rospy.loginfo('Small object handler: could not create a path.')
            self._as.set_succeeded(HandleObjectResult(0))



if __name__ == '__main__':
    rospy.init_node('small_object_handler')
    hs = HandleServer()
    rospy.spin()
