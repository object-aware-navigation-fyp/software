#!/usr/bin/env python
import rospy
import actionlib
import numpy as np
from object_aware_move_base.msg import HandleObjectAction, HandleObjectResult
from nav_msgs.srv import GetPlan
from nav_msgs.msg import Path
from visualization_msgs.msg import MarkerArray

class HandleServer:
    def __init__(self):
        self.timeout = rospy.Duration(5.0)
        self.inflation_radius = 0.27
        self.max_intersections = 2

        if rospy.has_param('/object_aware_navigation/known_object_inflation_radius'):
            self.inflation_radius = rospy.get_param('/object_aware_navigation/known_object_inflation_radius')
        if rospy.has_param('/object_aware_navigation/max_intersections'):
            self.max_intersections = rospy.get_param('/object_aware_navigation/max_intersections')

        rospy.wait_for_service('/move_base/GlobalPlanner/make_plan')
        self.get_plan = rospy.ServiceProxy('/move_base/GlobalPlanner/make_plan', GetPlan)

        self._as = actionlib.SimpleActionServer('/object_aware_move_base/handlers/person', HandleObjectAction, execute_cb=self.execute_cb, auto_start=False)
        self._as.start()


    def execute_cb(self, goal):
        rospy.loginfo('Person handler: handling at x:{:.2f} y:{:.2f} z:{:.2f}'.format(
            goal.marker.pose.position.x,
            goal.marker.pose.position.y,
            goal.marker.pose.position.z
        ))

        # get the last published plan
        # and use speech to alert the person
        last_plan = rospy.wait_for_message('/move_base/current_plan', Path)
        print '\033[92mTIAGo: pls go away\033[00m'

        if len(last_plan.poses):
            # get start and goal poses
            self.start = last_plan.poses[0]
            self.goal = last_plan.poses[-1]

            # start time and valid path count
            start_time = rospy.Time.now()
            count = 0

            # try to find a path until timeout with 0 valid paths
            # or a valid path was found 3 times in a row.
            while (rospy.Time.now() - start_time < self.timeout or count) and count < 3:
                if not self.check_for_collision():
                    count += 1
                else:
                    count = 0

        # success if count is non-zero
        if count:
            rospy.loginfo('Person handler: found a clearing.')
            rospy.sleep(2.0)
            self._as.set_succeeded(HandleObjectResult(1))
        else:
            rospy.loginfo('Person handler: could not find a path.')
            self._as.set_succeeded(HandleObjectResult(0))


    def check_for_collision(self, min_dist=2.0, max_dist=5.0):
            path = self.get_plan(self.start, self.goal, 1.0).plan
            marker_array = rospy.wait_for_message('/semantic_map/markers', MarkerArray)
            collision = False

            for posestamped in path.poses:
                # get distance between first and current point
                x0 = path.poses[0].pose.position.x
                y0 = path.poses[0].pose.position.y
                xk = posestamped.pose.position.x
                yk = posestamped.pose.position.y
                dist = np.linalg.norm(np.array([x0, y0]) - np.array([xk, yk]))

                # track number of inflation radii entered
                inflation_radii_entered = 0

                # check for collisions
                for marker in marker_array.markers:
                    mx = marker.pose.position.x
                    my = marker.pose.position.y
                    mw = marker.scale.x/2.0
                    mh = marker.scale.y/2.0

                    mx1 = mx - mw
                    my1 = my - mh
                    mx2 = mx + mw
                    my2 = my + mh

                    mx1_inflated = mx1 - self.inflation_radius
                    my1_inflated = my1 - self.inflation_radius
                    mx2_inflated = mx2 + self.inflation_radius
                    my2_inflated = my2 + self.inflation_radius

                    if dist < max_dist and dist > self.inflation_radius:

                        # test entry into inflation radius
                        if xk > mx1_inflated and xk < mx2_inflated and yk > my1_inflated and yk < my2_inflated:
                            inflation_radii_entered += 1

                            # if within the inflation radii of two known objects
                            # or in direct collision with a known object
                            if (inflation_radii_entered >= self.max_intersections) or (xk > mx1 and xk < mx2 and yk > my1 and yk < my2):

                                # collision if within minimum distance
                                if dist < min_dist:
                                    collision = True

                                break
                
                # stop checking if collision found
                if collision:
                    break

            return collision



if __name__ == '__main__':
    rospy.init_node('person_handler')
    hs = HandleServer()
    rospy.spin()
