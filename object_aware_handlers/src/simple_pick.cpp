#include <ros/ros.h>
#include <cmath>
#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include "object_aware_handlers/Pick.h"

moveit::planning_interface::MoveGroupInterface* group_arm_torso;
moveit::planning_interface::PlanningSceneInterface* planning_scene_interface;

const double GRIPPER_LENGTH = 0.21875;


void add_collision_box(const visualization_msgs::Marker &marker)
{
    moveit_msgs::CollisionObject object;
    object.header.frame_id = marker.header.frame_id;
    object.id = std::to_string((int)marker.id);

    shape_msgs::SolidPrimitive primitive;
    primitive.type = primitive.BOX;
    primitive.dimensions.resize(3);
    primitive.dimensions[0] = marker.scale.x;
    primitive.dimensions[1] = marker.scale.y;
    primitive.dimensions[2] = marker.scale.z;

    object.primitives.push_back(primitive);
    object.primitive_poses.push_back(marker.pose);
    object.operation = object.ADD;

    planning_scene_interface->applyCollisionObject(object);
}


moveit_msgs::Grasp top_grasp(const visualization_msgs::Marker &marker) {
    moveit_msgs::Grasp grasp;
    grasp.id = "grasp_" + marker.ns;

    grasp.pre_grasp_posture.joint_names.resize(2);
    grasp.pre_grasp_posture.joint_names[0] = "gripper_left_finger_joint";
    grasp.pre_grasp_posture.joint_names[1] = "gripper_right_finger_joint";
    grasp.pre_grasp_posture.points.resize(1);
    grasp.pre_grasp_posture.points[0].positions.resize(2);
    grasp.pre_grasp_posture.points[0].positions[0] = 0.05;
    grasp.pre_grasp_posture.points[0].positions[1] = 0.05;
    grasp.pre_grasp_posture.points[0].time_from_start = ros::Duration(0.5);

    // note: the rosparams
    //     /gripper_controller/constraints/gripper_left_finger_joint/goal
    //     /gripper_controller/constraints/gripper_right_finger_joint/goal
    // must be sufficiently large to reach this goal; set in
    //     /opt/pal/erbium/share/tiago_controller_configuration/config/pal-gripper_joint_trajectory_controllers.yaml
    grasp.grasp_posture.joint_names.resize(2);
    grasp.grasp_posture.joint_names[0] = "gripper_left_finger_joint";
    grasp.grasp_posture.joint_names[1] = "gripper_right_finger_joint";
    grasp.grasp_posture.points.resize(1);
    grasp.grasp_posture.points[0].positions.resize(2);
    grasp.grasp_posture.points[0].positions[0] = 0.00;
    grasp.grasp_posture.points[0].positions[1] = 0.00;
    grasp.grasp_posture.points[0].time_from_start = ros::Duration(0.5);

    grasp.grasp_pose.header.frame_id = marker.header.frame_id;
    grasp.grasp_pose.pose.orientation = tf::createQuaternionMsgFromRollPitchYaw(M_PI/2,M_PI/2,0);
    grasp.grasp_pose.pose.position.x = marker.pose.position.x;
    grasp.grasp_pose.pose.position.y = marker.pose.position.y;
    grasp.grasp_pose.pose.position.z = marker.pose.position.z + GRIPPER_LENGTH;

    grasp.pre_grasp_approach.direction.header.frame_id = "base_footprint";
    grasp.pre_grasp_approach.direction.vector.x = 0.0;
    grasp.pre_grasp_approach.direction.vector.y = 0.0;
    grasp.pre_grasp_approach.direction.vector.z = -0.3;
    grasp.pre_grasp_approach.min_distance = 0.095;
    grasp.pre_grasp_approach.desired_distance = 0.9;

    grasp.post_grasp_retreat.direction.header.frame_id = "base_footprint";
    grasp.post_grasp_retreat.direction.vector.x = 0.0;
    grasp.post_grasp_retreat.direction.vector.y = 0.0;
    grasp.post_grasp_retreat.direction.vector.z = 1.0;
    grasp.post_grasp_retreat.min_distance = 0.1;
    grasp.post_grasp_retreat.desired_distance = 0.2;

    return grasp;
}


void marker_array_callback(const visualization_msgs::MarkerArray::ConstPtr& markers)
{
    planning_scene_interface->removeCollisionObjects(planning_scene_interface->getKnownObjectNames());
    for (std::vector<visualization_msgs::Marker>::const_iterator marker = markers->markers.begin(); marker != markers->markers.end(); ++marker)
        add_collision_box(*marker);
}


bool pick(object_aware_handlers::Pick::Request &req, object_aware_handlers::Pick::Response &res)
{
    group_arm_torso->detachObject();
    planning_scene_interface->removeCollisionObjects(planning_scene_interface->getKnownObjectNames());

    add_collision_box(req.marker);
    ros::Duration(1.0).sleep();

    const std::string object = std::to_string((int)req.marker.id);
    moveit_msgs::Grasp grasp = top_grasp(req.marker);

    res.error = (bool)group_arm_torso->pick(object, grasp);
    std::cout << "ERROR CODE: " << res.error << std::endl;

    return true;
}



int main(int argc, char ** argv) {
    ros::init(argc, argv, "plan_arm_torsooo");

    group_arm_torso = new moveit::planning_interface::MoveGroupInterface("arm_torso");
    planning_scene_interface = new moveit::planning_interface::PlanningSceneInterface();

    ros::AsyncSpinner spinner(0);
    ros::NodeHandle n;
    // ros::Subscriber marker_sub = n.subscribe("/semantic_map/markers", 1000, marker_array_callback);
    ros::ServiceServer pick_svc = n.advertiseService("pick_known_object", pick);

    spinner.start();
    ros::waitForShutdown();
}