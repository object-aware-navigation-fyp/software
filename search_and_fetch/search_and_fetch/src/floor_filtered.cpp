/**
 * Floor Filtered
 * Removes the floor from a throttled PointCloud2
 * 
 * pcl reference: http://pointclouds.org/documentation/tutorials/
 */

#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <tf/transform_listener.h>
#include <pcl_ros/transforms.h>
#include <pcl/filters/conditional_removal.h>


// listener to transform cloud onto base_footprint
tf::TransformListener* listener;
ros::Publisher cloud_pub;

void callback(const sensor_msgs::PointCloud2::ConstPtr& msg) {
    pcl::PointCloud<pcl::PointXYZ>::Ptr xtion_frame_cloud(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered(new pcl::PointCloud<pcl::PointXYZ>);

    // pcl from msg and transform
    pcl::fromROSMsg(*msg, *xtion_frame_cloud);
    pcl_ros::transformPointCloud("/base_link", *xtion_frame_cloud, *cloud, *listener);

    // filter z > 5cm (also removes NaNs)
    pcl::ConditionOr<pcl::PointXYZ>::Ptr range_cond(new pcl::ConditionOr<pcl::PointXYZ>());    
    range_cond->addComparison(pcl::FieldComparison<pcl::PointXYZ>::ConstPtr (
        new pcl::FieldComparison<pcl::PointXYZ>("z", pcl::ComparisonOps::GT, 0.05)
    ));

    // build and apply filter
    pcl::ConditionalRemoval<pcl::PointXYZ> condrem;
    condrem.setCondition(range_cond);
    condrem.setInputCloud(cloud);
    condrem.setKeepOrganized(false);
    condrem.filter(*cloud_filtered);
    cloud.swap(cloud_filtered);

    // pcl -> pcl2msg
    sensor_msgs::PointCloud2 msg_out;
    pcl::toROSMsg(*cloud, msg_out);
    msg_out.header.frame_id = "/base_link";
    msg_out.header.stamp = msg->header.stamp;

    cloud_pub.publish(msg_out);
}

int main(int argc, char **argv) {
    ros::init(argc, argv, "floor_filtered");
    listener = new tf::TransformListener();
    ros::Duration(2.0).sleep(); // populate the transform listener
    ros::NodeHandle n;
    
    cloud_pub = n.advertise<sensor_msgs::PointCloud2>("/throttle_filtering_points/floor_filtered_points", 10);
    ros::Subscriber cloud_sub = n.subscribe("/throttle_filtering_points/filtered_points", 10, callback);

    ros::spin();
}
