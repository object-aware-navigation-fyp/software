import rospy
import actionlib
from frontier_exploration.msg import ExploreTaskAction, ExploreTaskGoal
from geometry_msgs.msg import Point32

rospy.init_node('send_frontier')
client = actionlib.SimpleActionClient('/explore_server', ExploreTaskAction)
client.wait_for_server()

goal = ExploreTaskGoal()
goal.explore_boundary.header.stamp = rospy.Time.now()
goal.explore_boundary.header.frame_id = 'map'
goal.explore_boundary.polygon.points.append(Point32(-0.86, -0.71, 0))
goal.explore_boundary.polygon.points.append(Point32(-0.60, 5.05, 0))
goal.explore_boundary.polygon.points.append(Point32(3.14, 5.02, 0))
goal.explore_boundary.polygon.points.append(Point32(3.14, -0.69, 0))
goal.explore_center.header.stamp = rospy.Time.now()
goal.explore_center.header.frame_id = 'map'
goal.explore_center.point.x = 0.51
goal.explore_center.point.y = 0.80
goal.explore_center.point.z = 0.0

client.send_goal(goal)
print 'waiting'
client.wait_for_result()