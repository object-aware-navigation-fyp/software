#!/usr/bin/env python
import rospy
from sensor_msgs.msg import LaserScan

def laser_callback(laser):
    # sizes and mid point
    size_half = int(len(laser.ranges) * 0.1)/2
    mid = int(len(laser.ranges)/2)

    # take a slice of the laser
    laser.ranges = laser.ranges[mid - size_half : mid] + laser.ranges[mid : mid + size_half]
    laser.intensities = laser.intensities[mid - size_half : mid] + laser.intensities[mid : mid + size_half]
    laser.angle_min = -laser.angle_increment * size_half
    laser.angle_max = laser.angle_increment * size_half

    laser_pub.publish(laser)

 
rospy.init_node('laser_sliced')
laser_sub = rospy.Subscriber('/scan', LaserScan, laser_callback)
laser_pub = rospy.Publisher('/scan_sliced', LaserScan, queue_size=1)
rospy.spin()