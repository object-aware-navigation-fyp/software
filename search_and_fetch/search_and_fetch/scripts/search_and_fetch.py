#!/usr/bin/env python
import rospy
import numpy as np
import actionlib

from frontier_exploration.msg import ExploreTaskAction, ExploreTaskGoal
from nav_msgs.msg import OccupancyGrid
from geometry_msgs.msg import Point32, PoseStamped, PoseWithCovarianceStamped

from visualization_msgs.msg import MarkerArray
from control_msgs.msg import FollowJointTrajectoryAction, FollowJointTrajectoryGoal
from trajectory_msgs.msg import JointTrajectoryPoint
from semantic_navigation.semantic_navigation import SemanticNavigation
from object_aware_handlers.srv import Pick

from copy import deepcopy

KNOWN_OBJECTS = ['coffee']


def get_min_max_occupancy_grid(grid):
    """
    minmax points of an occupancy grid in metres.
    output: x1,y1,x2,y2
    """
    image = np.array(grid.data, dtype=np.int8)
    image = image.reshape(grid.info.height, grid.info.width)

    # obstacles are 100 on the map
    obstacles = np.argwhere(image > 0)

    x1 = np.amin(obstacles[:,1])
    y1 = np.amin(obstacles[:,0])
    x2 = np.amax(obstacles[:,1])
    y2 = np.amax(obstacles[:,0])

    # convert to metres
    x1 = x1 * grid.info.resolution + grid.info.origin.position.x
    y1 = y1 * grid.info.resolution + grid.info.origin.position.y
    x2 = x2 * grid.info.resolution + grid.info.origin.position.x
    y2 = y2 * grid.info.resolution + grid.info.origin.position.y

    return x1,y1,x2,y2


def send_joint_trajectory(joint_names, position, time_from_start):
    head_trajectory_client = actionlib.SimpleActionClient('/head_controller/follow_joint_trajectory', FollowJointTrajectoryAction)

    goal = FollowJointTrajectoryGoal()
    goal.trajectory.joint_names = joint_names

    # set first (and only) position
    goal.trajectory.points.append(JointTrajectoryPoint())
    goal.trajectory.points[0].positions = position
    goal.trajectory.points[0].time_from_start = rospy.Duration(time_from_start)

    head_trajectory_client.wait_for_server()
    head_trajectory_client.send_goal(goal)

    return head_trajectory_client.wait_for_result, head_trajectory_client.get_result


def fetch(target):
    navigator = SemanticNavigation()
    rospy.wait_for_service('pick_known_object')
    pick = rospy.ServiceProxy('pick_known_object', Pick)

    # get start pose
    pose = rospy.wait_for_message('/amcl_pose', PoseWithCovarianceStamped)
    start = PoseStamped()
    start.header = pose.header
    start.pose = pose.pose.pose

    # go to the object
    wait_for_result, get_result = navigator.move_to_marker(start, target, 0.5)
    while not wait_for_result(rospy.Duration(0.1)):
        markers = rospy.wait_for_message('/semantic_map/markers', MarkerArray)
        for marker in markers.markers:
            if marker.ns == target.ns:
                target = marker
                navigator.point_head_to_marker(target, 1.0)
                move = navigator.move_to_marker(start, deepcopy(target), 0.5)
                if move[0] is not None:
                    wait_for_result, get_result = move

    # try to pick it up
    rospy.sleep(2.0)
    result = pick(target)
    return result.error


if __name__ == '__main__':
    rospy.init_node('send_frontier')
    client = actionlib.SimpleActionClient('/explore_server', ExploreTaskAction)
    client.wait_for_server()

    # look down
    send_joint_trajectory(['head_1_joint', 'head_2_joint'], [0.0, -0.5], 2.0)

    # get boundary and robot x,y coordinates
    map_grid = rospy.wait_for_message('/map', OccupancyGrid)
    robot_pose = rospy.wait_for_message('/amcl_pose', PoseWithCovarianceStamped)
    x1,y1,x2,y2 = get_min_max_occupancy_grid(map_grid)

    # exploration goal
    goal = ExploreTaskGoal()
    goal.explore_boundary.header.stamp = rospy.Time.now()
    goal.explore_boundary.header.frame_id = 'map'
    goal.explore_boundary.polygon.points.append(Point32(x1,y1,0))
    goal.explore_boundary.polygon.points.append(Point32(x1,y2,0))
    goal.explore_boundary.polygon.points.append(Point32(x2,y2,0))
    goal.explore_boundary.polygon.points.append(Point32(x2,y1,0))
    goal.explore_center.header.stamp = rospy.Time.now()
    goal.explore_center.header.frame_id = 'map'
    goal.explore_center.point = robot_pose.pose.pose.position

    target = None

    client.send_goal(goal)
    while not rospy.is_shutdown() and not client.wait_for_result(rospy.Duration(0.1)):
        # search for object
        markers = rospy.wait_for_message('/semantic_map/markers', MarkerArray)
        for marker in markers.markers:
            if len(KNOWN_OBJECTS) == 0 or marker.ns in KNOWN_OBJECTS:
                target = marker

        if target is not None:
            client.cancel_all_goals()
            rospy.sleep(1.0)
            fetch(target)
            break
