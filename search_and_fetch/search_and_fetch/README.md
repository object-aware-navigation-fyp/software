# Search and Fetch
Simple demonstration of "Go Get It!" proposed in the RoboCup@Home 2010 Rulebook.

## Quick Start
The mobile robot searches a previously mapped room for a coffee cup, navigating to and picking it up once located.

    roslaunch semantic_mapping semantic_mapping.launch use_yolo:=1
    roslaunch search_and_fetch search_and_fetch.launch
    rosrun search_and_fetch search_and_fetch.py

## Credits
### Frontier Exploration (ROS)
http://wiki.ros.org/frontier_exploration

### RoboCup@Home
http://www.robocupathome.org/