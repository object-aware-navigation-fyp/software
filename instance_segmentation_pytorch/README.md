# Instance Segmentation with PyTorch
## Instance Segmentation ROS Service Definition
Request/Input:

| Name    |        Type       | Description                |
|---------|:-----------------:|----------------------------|
| dataset | string            | weights used for detection |
| image   | sensor_msgs/Image | input image                |

Response/Output:

| Name       |         Type         | Description                           |
|------------|:--------------------:|---------------------------------------|
| count      | int32                | number of detections                  |
| boxes      | float32[ ]           | bounding boxes (x1,y1,x2,y2), ordered |
| labels     | int32[ ]             | integer labels, ordered               |
| labels_txt | string[ ]            | string labels, ordered                |
| scores     | float32[ ]           | detection scores, ordered             |
| masks      | sensor_msgs/Image[ ] | segmentation masks, ordered           |

## Mask R-CNN
ROS service and catkin python object for Mask R-CNN object instance segmentation built using the torchvision implementation of Mask R-CNN.

### Installation
    pip install torch==1.4.0 torchvision==0.5.0 numpy==1.16.5

### Quick start
    rosrun instance_segmentation_pytorch mask_rcnn_node.py

optionally from within a python script:

    from object_segmentation_mask_rcnn import MaskRCNN
    
### Pre-trained models
Directory structure for loading pre-trained models:

    object_detection_mask_rcnn
    |__ models
        |__ my_dataset
            |__ _labels.txt
            |__ model.pth

The `_default` model trained on the MS COCO dataset is available through torchvision: https://pytorch.org/docs/stable/torchvision/models.html

## YOLACT (Experimental)
ROS Service for YOLACT instance segmentation built using the official PyTorch implementation.

Functionality is experimental since ROS Kinetic officially supports python 2.7 and a number of workarounds need to be used to get YOLACT (python 3) up and running with rospy.

### Installation
    pip3 install numpy==1.16.5 pillow==0.5 torch==1.4.0 torchvision==0.5 python-opencv==4.0.0.21 matplotlib==2.1.0 pycocotools

### Quick start
    rosrun instance_segmentation_pytorch yolact_node.py

### Pre-trained models
Directory structure for loading pre-trained models:

    instance_segmentation_yolact
    |__ yolact
        |__ weights
            |__ yolact_base_54_800000.pth

The official YOLACT weights trained on MS COCO with a Resnet101 backbone are supported.

Weights available at: https://drive.google.com/file/d/1UYy3dMapbH1BnmtZU4WH1zbYgOzzHHf_/view?usp=sharing.

## Credits
torchvision: https://pytorch.org/docs/stable/torchvision/index.html

Mask R-CNN: https://arxiv.org/abs/1703.06870

YOLACT: https://github.com/dbolya/yolact