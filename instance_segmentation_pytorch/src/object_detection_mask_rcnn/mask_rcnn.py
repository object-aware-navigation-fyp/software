#!/usr/bin/env python
import torch
import torchvision
import torchvision.transforms as transforms
if torch.cuda.is_available():
    DEVICE = 'cuda'
else:
    DEVICE = 'cpu'

import os.path
from PIL import Image


def mask_rcnn_transform():
    return transforms.Compose([
        transforms.ToTensor(),
    ])


class MaskRCNN:
    def __init__(self, model_path):
        self.model_path = None
        self.transform = mask_rcnn_transform()
        self.load_model(model_path)


    def load_model(self, model_path):
        """ Load model
        loads Mask R-CNN model weights and labels.
        """
        # raise exception if files do not exist
        if not os.path.isdir(model_path):
            raise IOError('Model directory not found: {}'.format(model_path))
        if not os.path.isfile(os.path.join(model_path, 'model.pth')):
            raise IOError('model.pth not found in model directory: {}'.format(model_path))
        if not os.path.isfile(os.path.join(model_path, '_labels.txt')):
            raise IOError('_labels.txt not found in model directory: {}'.format(model_path))

        # load Mask R-CNN weights
        self.model_path = model_path
        weights = torch.load(os.path.join(self.model_path, 'model.pth'))
        self.net = torchvision.models.detection.maskrcnn_resnet50_fpn()
        self.net.load_state_dict(weights)
        self.net.to(DEVICE)
        self.net.eval()

        # parse text labels
        self.labels = open(os.path.join(self.model_path, '_labels.txt')).read().strip().split('\n')


    def forward(self, model_path, frame):
        """ Forward pass
        runs a forward pass of Mask R-CNN on an input frame.

        PARAMS: model_path  path to directory containing "model.pth"
                frame       bgr cv2 image

        RETURN: boxes       bounding boxes x1,y1,x2,y2
                labels      numbered labels
                labels_txt  text labels
                masks       segmentation masks for each detection
        """
        # load model if it is not already up
        if not self.model_path == model_path:
            self.load_model(model_path)

        assert self.net is not None
        
        # preprocess
        image = Image.fromarray(frame[:, :, ::-1]) # rgb -> bgr -> PIL
        image = torch.stack([self.transform(image)]).to(DEVICE)

        # net forward
        predictions = self.net(image)
        boxes = predictions[0]['boxes'].cpu().detach().numpy()
        labels = predictions[0]['labels'].cpu().detach().numpy()
        scores = predictions[0]['scores'].cpu().detach().numpy()
        masks = predictions[0]['masks'].permute(0,2,3,1).cpu().detach().numpy()

        # get text labels
        labels_txt = [self.labels[label] for label in labels]

        return boxes, labels, labels_txt, scores, masks