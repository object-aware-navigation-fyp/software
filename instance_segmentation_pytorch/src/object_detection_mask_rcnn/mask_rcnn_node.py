#!/usr/bin/env python
import rospy
from sensor_msgs.msg import Image
from instance_segmentation_pytorch.srv import InstanceSegmentation as MaskRcnnSrv
from cv_bridge import CvBridge
import rospkg
MODEL_ROOT = rospkg.RosPack().get_path('instance_segmentation_pytorch') + '/models'

from mask_rcnn import MaskRCNN


class object_detection_server:
    def __init__(self):
        self.bridge = CvBridge()
        self.mask_rcnn = MaskRCNN('{}/{}'.format(MODEL_ROOT, '_default'))

    def mask_rcnn_request(self, req):
        # convert image msg to cv2 bgr frame
        # and forward pass through network
        frame = self.bridge.imgmsg_to_cv2(req.image)
        boxes, labels, labels_txt, scores, masks = self.mask_rcnn.forward('{}/{}'.format(MODEL_ROOT, req.dataset), frame)

        # reformat data for .srv
        count = masks.shape[0]
        boxes = boxes.flatten()
        labels = labels.flatten()
        scores = scores.flatten()
        masks_imgmsg = []
        for mask in masks:
            masks_imgmsg.append(self.bridge.cv2_to_imgmsg(mask, encoding='32FC1'))
        
        return count,boxes,labels,labels_txt,scores,masks_imgmsg


if __name__ == '__main__':
    rospy.init_node('mask_rcnn')
    rcnn = object_detection_server()
    service = rospy.Service('mask_rcnn/forward', MaskRcnnSrv, rcnn.mask_rcnn_request)
    rospy.loginfo('Mask-RCNN service initialised!')
    rospy.spin()