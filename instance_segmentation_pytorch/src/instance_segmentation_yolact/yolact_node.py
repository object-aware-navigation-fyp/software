#!/usr/bin/python3
import rospy
from sensor_msgs.msg import Image
from instance_segmentation_pytorch.srv import InstanceSegmentation
# from cv_bridge import CvBridge
import rospkg

# ROS Kinetic? python2.7???
import sys
sys.path.remove('/opt/ros/kinetic/lib/python2.7/dist-packages')

# torch and GPU stuff
import torch
torch.set_default_tensor_type('torch.cuda.FloatTensor')

# YOLACT
import rospkg
YOLACT_PATH = rospkg.RosPack().get_path('instance_segmentation_pytorch') + '/src/instance_segmentation_yolact/yolact'
sys.path.append(YOLACT_PATH)
from utils.augmentations import FastBaseTransform
from layers.output_utils import postprocess, undo_image_transformation
from yolact import Yolact
from data import cfg
cfg.mask_proto_debug = 0

import numpy as np

# COCO labels taken from https://pytorch.org/vision/stable/models.html
COCO_INSTANCE_CATEGORY_NAMES = [
    'person', 'bicycle', 'car', 'motorcycle', 'airplane', 'bus',
    'train', 'truck', 'boat', 'traffic light', 'fire hydrant', 'N/A', 'stop sign',
    'parking meter', 'bench', 'bird', 'cat', 'dog', 'horse', 'sheep', 'cow',
    'elephant', 'bear', 'zebra', 'giraffe', 'N/A', 'backpack', 'umbrella', 'N/A', 'N/A',
    'handbag', 'tie', 'suitcase', 'frisbee', 'skis', 'snowboard', 'sports ball',
    'kite', 'baseball bat', 'baseball glove', 'skateboard', 'surfboard', 'tennis racket',
    'bottle', 'N/A', 'wine glass', 'cup', 'fork', 'knife', 'spoon', 'bowl',
    'banana', 'apple', 'sandwich', 'orange', 'broccoli', 'carrot', 'hot dog', 'pizza',
    'donut', 'cake', 'chair', 'couch', 'potted plant', 'bed', 'N/A', 'dining table',
    'N/A', 'N/A', 'toilet', 'N/A', 'tv', 'laptop', 'mouse', 'remote', 'keyboard', 'cell phone',
    'microwave', 'oven', 'toaster', 'sink', 'refrigerator', 'N/A', 'book',
    'clock', 'vase', 'scissors', 'teddy bear', 'hair drier', 'toothbrush'
]

def prep_benchmark(dets_out, h, w, top_k=15, score_threshold=0):
    """
    post-process results from YOLACT.
    taken from https://github.com/dbolya/yolact.
    """
    t = postprocess(dets_out, w, h, crop_masks=0, score_threshold=score_threshold)

    classes, scores, boxes, masks = [x[:top_k] for x in t]
    if isinstance(scores, list):
        box_scores = scores[0].cpu().numpy()
        mask_scores = scores[1].cpu().numpy()
    else:
        scores = scores.cpu().numpy()
    classes = classes.cpu().numpy()
    boxes = boxes.cpu().numpy()
    masks = masks.cpu().numpy()

    # Just in case
    torch.cuda.synchronize()

    return scores, classes, boxes, masks


class object_detection_server:
    def __init__(self):
        # self.bridge = CvBridge()
        self.net = Yolact()
        self.net.load_weights(YOLACT_PATH + '/weights/yolact_base_54_800000.pth')
        self.net.eval()
        self.net = self.net.cuda()

    def yolact_request(self, req):
        # convert image msg to cv2 bgr frame
        # NO CV_BRIDGE )':
        frame = np.fromstring(req.image.data, dtype=np.uint8)
        frame = frame.reshape(req.image.height, req.image.width, -1)
        # frame = self.bridge.imgmsg_to_cv2(req.image)

        # pre-process
        h,w,c = frame.shape
        frame = torch.from_numpy(frame).cuda().float()
        batch = FastBaseTransform()(frame.unsqueeze(0))

        # forward pass and post-process
        with torch.no_grad():
            preds = self.net(batch)
        scores, labels, boxes, masks = prep_benchmark(preds, h, w, score_threshold=0.1)

        # reformat data for .srv
        count = masks.shape[0]
        boxes = boxes.flatten()
        labels = labels.flatten()
        scores = scores.flatten()
        labels_txt = [COCO_INSTANCE_CATEGORY_NAMES[label] for label in labels]

        # masks array
        masks_imgmsg = []

        # default mask imgmsg
        mask_imgmsg = Image()
        mask_imgmsg.header = req.image.header
        mask_imgmsg.height = req.image.height
        mask_imgmsg.width = req.image.width
        mask_imgmsg.encoding = '32FC1'
        mask_imgmsg.is_bigendian = req.image.is_bigendian
        mask_imgmsg.step = 4 * mask_imgmsg.width

        # populate masks array
        for mask in masks:
            # NO CV_BRIDGE AGAIN )':
            mask_imgmsg.data = mask.tostring()
            masks_imgmsg.append(mask_imgmsg)
            # masks_imgmsg.append(self.bridge.cv2_to_imgmsg(mask, encoding='32FC1'))
        
        return count,boxes,labels,labels_txt,scores,masks_imgmsg


if __name__ == '__main__':
    rospy.init_node('yolact')
    server = object_detection_server()
    service = rospy.Service('yolact/forward', InstanceSegmentation, server.yolact_request)
    rospy.loginfo('YOLACT service initialised!')
    rospy.spin()