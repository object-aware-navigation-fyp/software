import rospy
from geometry_msgs.msg import Vector3
from object_segmentation_pcl.srv import *


class SegmentationClient:
    ''' Segmentation Client
        convenience class for calling PCL segmentation services.
        provides functions with optional arguments and default
        values for all service calls.
    '''
    def __init__(self, namespace='/segmentation/'):
        self.namespace = namespace
        rospy.wait_for_service(namespace + 'remove_outliers'    , 5)
        rospy.wait_for_service(namespace + 'trim_cloud'         , 5)
        rospy.wait_for_service(namespace + 'downsample'         , 5)
        rospy.wait_for_service(namespace + 'get_bounding_cuboid', 5)
        rospy.wait_for_service(namespace + 'cluster_euclidean'  , 5)
        rospy.wait_for_service(namespace + 'segment_plane'      , 5)
        rospy.wait_for_service(namespace + 'segment_lccp'       , 5)
        rospy.wait_for_service(namespace + 'pipelines/cluster'  , 5)
        rospy.wait_for_service(namespace + 'pipelines/lccp'     , 5)
        self.remove_outliers_svc     = rospy.ServiceProxy(namespace + 'remove_outliers', RemoveOutliers)
        self.trim_cloud_svc          = rospy.ServiceProxy(namespace + 'trim_cloud', TrimCloud)
        self.downsample_svc          = rospy.ServiceProxy(namespace + 'downsample', Downsample)
        self.get_bounding_cuboid_svc = rospy.ServiceProxy(namespace + 'get_bounding_cuboid', GetBoundingCuboid)
        self.cluster_euclidean_svc   = rospy.ServiceProxy(namespace + 'cluster_euclidean', ClusterEuclidean)
        self.segment_plane_svc       = rospy.ServiceProxy(namespace + 'segment_plane', SegmentPlane)
        self.segment_lccp_svc        = rospy.ServiceProxy(namespace + 'segment_lccp', SegmentLCCP)
        self.pipeline_cluster_svc    = rospy.ServiceProxy(namespace + 'pipelines/cluster', PipelineCluster)
        self.pipeline_lccp_svc       = rospy.ServiceProxy(namespace + 'pipelines/lccp', PipelineLCCP)


    def remove_outliers(self, points, radius_search=0.8, min_neighbours=2):
        return self.remove_outliers_svc(points, radius_search, min_neighbours)


    def trim_cloud(self, points, top_x=0, top_y=0, top_z=0, bottom_x=0, bottom_y=0, bottom_z=0, set_negative=0):
        top = Vector3(top_x, top_y, top_z)
        bottom = Vector3(bottom_x, bottom_y, bottom_z)
        return self.trim_cloud_svc(points, top, bottom, set_negative)


    def downsample(self, points, leaf_size_x=0.01, leaf_size_y=0.01, leaf_size_z=0.01):
        leaf_size = Vector3(leaf_size_x, leaf_size_y, leaf_size_z)
        return self.downsample_svc(points, leaf_size)


    def get_bounding_cuboid(self, points):
        return self.get_bounding_cuboid_svc(points)


    def cluster_euclidean(self, points, cluster_tolerance=0.02, min_cluster_size=100, max_cluster_size=25000):
        return self.cluster_euclidean_svc(points, cluster_tolerance, min_cluster_size, max_cluster_size)


    def segment_plane(self, points, distance_threshold=0.01):
        return self.segment_plane_svc(points, distance_threshold)


    def segment_lccp(self,
                # input cloud
                points,
                # supervoxel params
                voxel_resolution=0.0075,
                seed_resolution=0.03,
                colour_importance=0.0,
                spatial_importance=1.0,
                normal_importance=4.0,
                use_single_cam_transform=0,
                use_supervoxel_refinement=0,
                # lccp params
                concavity_tolerance_threshold=10,
                smoothness_threshold=0.1,
                min_segment_size=0,
                use_extended_convexity=0,
                use_sanity_criterion=0,
                # output params
                return_clouds=[0,1,0,1]
            ):
        return self.segment_lccp_svc(points,
            voxel_resolution, seed_resolution, colour_importance, spatial_importance,
            normal_importance, use_single_cam_transform, use_supervoxel_refinement,
            concavity_tolerance_threshold, smoothness_threshold, min_segment_size,
            use_extended_convexity, use_sanity_criterion, return_clouds)


    def pipeline_cluster(self, points, target_frame,
                        leaf_size_x=0.01, leaf_size_y=0.01, leaf_size_z=0.01,
                        cluster_tolerance=0.02, min_cluster_size=100, max_cluster_size=25000
                    ):
        leaf_size = Vector3(leaf_size_x, leaf_size_y, leaf_size_z)
        rospy.wait_for_service(self.namespace + 'pipelines/cluster')
        return self.pipeline_cluster_svc(points, target_frame, leaf_size, cluster_tolerance, min_cluster_size, max_cluster_size)


    def pipeline_lccp(self,
                # input cloud
                points,
                target_frame,
                # supervoxel params
                voxel_resolution=0.0075,
                seed_resolution=0.03,
                colour_importance=0.0,
                spatial_importance=1.0,
                normal_importance=4.0,
                use_single_cam_transform=0,
                use_supervoxel_refinement=0,
                # lccp params
                concavity_tolerance_threshold=10,
                smoothness_threshold=0.1,
                min_segment_size=0,
                use_extended_convexity=0,
                use_sanity_criterion=0,
            ):
        rospy.wait_for_service(self.namespace + 'pipelines/lccp')
        return self.pipeline_lccp_svc(points, target_frame,
            voxel_resolution, seed_resolution, colour_importance, spatial_importance,
            normal_importance, use_single_cam_transform, use_supervoxel_refinement,
            concavity_tolerance_threshold, smoothness_threshold, min_segment_size,
            use_extended_convexity, use_sanity_criterion)


# test client
if __name__ == '__main__':
    try:
        segmentation_client = SegmentationClient()
    except rospy.exceptions.ROSException as e:
        print e