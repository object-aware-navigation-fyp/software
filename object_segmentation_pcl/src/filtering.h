#ifndef SC18J3J_FILTERING_H
#define SC18J3J_FILTERING_H

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <geometry_msgs/Vector3.h>

/** Downsample Point Cloud
 *
 * downsample the pointcloud using a voxel grid filter
 *
 * reference: https://pointclouds.org/documentation/tutorials/voxel_grid.html
 */
template <typename PointT>
void downsample(typename pcl::PointCloud<PointT>::Ptr cloud,
                typename pcl::PointCloud<PointT>::Ptr voxel_filtered_cloud,
                geometry_msgs::Vector3 leaf_size);


/** Remove outliers
 *
 * remove outliers using radius search 
 * and a threshold on neighbouring points
 *
 * reference: https://pointclouds.org/documentation/tutorials/remove_outliers.html
 */
void remove_outliers(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud,
                     pcl::PointCloud<pcl::PointXYZ>::Ptr outlier_filtered_cloud,
                     float radius_search,
                     int min_neighbours);


/** Trim Cloud
 *
 * trim top and bottom of cloud by user specified values
 *
 * returns points within the trimmed cloud if set_negative=false
 * returns points outside of the trimmed cloud if set_negative=true
 *
 * reference: https://pointclouds.org/documentation/tutorials/remove_outliers.html
 */
void trim_cloud(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud,
                pcl::PointCloud<pcl::PointXYZ>::Ptr filtered_cloud,
                geometry_msgs::Vector3 top,
                geometry_msgs::Vector3 bottom,
                bool set_negative);

#endif