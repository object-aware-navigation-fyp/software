#include "segmentation.h"

#include <pcl/segmentation/sac_segmentation.h> // SAC segmentation
#include <pcl/filters/extract_indices.h> // extract indices
#include <pcl/segmentation/supervoxel_clustering.h> // supervoxels
#include "lccp_segmentation.h" // lccp segmentation
#include "colors.h" // colours for glasbey


void segment_plane(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud,
                   pcl::PointCloud<pcl::PointXYZ>::Ptr plane_filtered_cloud,
                   float distance_threshold)
{
    pcl::PointCloud<pcl::PointXYZ>::Ptr nan_filtered_cloud(new pcl::PointCloud<pcl::PointXYZ>);

    // remove NaNs
    std::vector<int> nan_indices;
    pcl::removeNaNFromPointCloud(*cloud, *nan_filtered_cloud, nan_indices);
    cloud.swap(nan_filtered_cloud);

    // create SAC segmentation
    pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients);
    pcl::PointIndices::Ptr inliers(new pcl::PointIndices);
    pcl::SACSegmentation<pcl::PointXYZ> seg;
    seg.setModelType(pcl::SACMODEL_PLANE);
    seg.setMethodType(pcl::SAC_RANSAC);
    seg.setDistanceThreshold(distance_threshold);
    seg.setInputCloud(cloud);
    seg.segment(*inliers, *coefficients);

    // extract inliers
    pcl::ExtractIndices<pcl::PointXYZ> extract;
    extract.setInputCloud(cloud);
    extract.setIndices(inliers);
    extract.setNegative(true);
    extract.filter(*plane_filtered_cloud);
}



void segment_lccp(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr& cloud,
                  pcl::PointCloud<pcl::PointXYZRGB>::Ptr& sv_coloured_cloud,
                  pcl::PointCloud<pcl::PointXYZL>::Ptr& sv_labelled_cloud,
                  pcl::PointCloud<pcl::PointXYZRGB>::Ptr& lccp_coloured_cloud,
                  pcl::PointCloud<pcl::PointXYZL>::Ptr& lccp_labelled_cloud,
                  float voxel_resolution,
                  float seed_resolution,
                  float colour_importance,
                  float spatial_importance,
                  float normal_importance,
                  bool use_single_cam_transform,
                  bool use_supervoxel_refinement,
                  float concavity_tolerance_threshold,
                  float smoothness_threshold,
                  int min_segment_size,
                  bool use_extended_convexity,
                  bool use_sanity_criterion)
{
    // supervoxel clustering
    pcl::SupervoxelClustering<pcl::PointXYZRGBA> super (voxel_resolution, seed_resolution, use_single_cam_transform);
    std::map<uint32_t, pcl::Supervoxel<pcl::PointXYZRGBA>::Ptr> supervoxel_clusters;
    super.setInputCloud(cloud);
    super.setColorImportance(colour_importance);
    super.setSpatialImportance(spatial_importance);
    super.setNormalImportance(normal_importance);
    super.extract(supervoxel_clusters);

    // supervoxel refinement
    if (use_supervoxel_refinement)
        super.refineSupervoxels(2, supervoxel_clusters);

    // supervoxel adjacency
    std::multimap<uint32_t, uint32_t> supervoxel_adjacency;
    super.getSupervoxelAdjacency (supervoxel_adjacency);

    // lccp k factor
    uint k_factor = (uint)use_extended_convexity;

    // lccp segmentation
    pcl::LCCPSegmentation<pcl::PointXYZRGBA> lccp;
    lccp.setConcavityToleranceThreshold (concavity_tolerance_threshold);
    lccp.setSanityCheck (use_sanity_criterion);
    lccp.setSmoothnessCheck (true, voxel_resolution, seed_resolution, smoothness_threshold);
    lccp.setKFactor (k_factor);
    lccp.segment (supervoxel_clusters, supervoxel_adjacency);
    if (min_segment_size > 0)
        lccp.removeSmallSegments(min_segment_size);

    // create sv coloured cloud (in RGB so we can view this on rviz)
    // a workaround for RGB is to change the ROSMsg's PointField[], but this could be risky
    pcl::copyPointCloud(*super.getColoredCloud(), *sv_coloured_cloud);

    // create lccp labelled cloud
    sv_labelled_cloud = super.getLabeledCloud ();
    lccp_labelled_cloud = sv_labelled_cloud->makeShared ();
    lccp.relabelCloud (*lccp_labelled_cloud);

    // create lccp coloured cloud
    pcl::copyPointCloud(*sv_coloured_cloud, *lccp_coloured_cloud);
    int lccp_index = 0;
    for(pcl::PointCloud<pcl::PointXYZRGB>::iterator point_xyzrgb = lccp_coloured_cloud->begin(); point_xyzrgb != lccp_coloured_cloud->end(); ++point_xyzrgb)
    {
        pcl::PointXYZL point_xyzl = lccp_labelled_cloud->at(lccp_index);
        unsigned int label = point_xyzl.label;
        pcl::RGB colour;
        if (label == 0)
        colour = pcl::RGB();
        else
        colour = pcl::getGlasbeyColor(label % pcl::GLASBEY_LUT_SIZE);
        point_xyzrgb->x = point_xyzl.x;
        point_xyzrgb->y = point_xyzl.y;
        point_xyzrgb->z = point_xyzl.z;
        point_xyzrgb->r = colour.r;
        point_xyzrgb->g = colour.g;
        point_xyzrgb->b = colour.b;
        ++lccp_index;
    }
}
