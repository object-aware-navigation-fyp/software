#ifndef SC18J3J_SERVER_H
#define SC18J3J_SERVER_H

#include <object_segmentation_pcl/RemoveOutliers.h>
#include <object_segmentation_pcl/TrimCloud.h>
#include <object_segmentation_pcl/Downsample.h>
#include <object_segmentation_pcl/GetBoundingCuboid.h>
#include <object_segmentation_pcl/ClusterEuclidean.h>
#include <object_segmentation_pcl/SegmentPlane.h>
#include <object_segmentation_pcl/SegmentLCCP.h>
#include <object_segmentation_pcl/PipelineCluster.h>
#include <object_segmentation_pcl/PipelineLCCP.h>

/** Service handlers
 *
 * provide ROS services for PCL functions implemented in this package
 *
 **/
template <typename PointT>
void call_downsample(object_segmentation_pcl::Downsample::Request &req, object_segmentation_pcl::Downsample::Response &res);

bool handle_downsample(object_segmentation_pcl::Downsample::Request &req, object_segmentation_pcl::Downsample::Response &res);

bool handle_remove_outliers(object_segmentation_pcl::RemoveOutliers::Request &req, object_segmentation_pcl::RemoveOutliers::Response &res);

bool handle_trim_cloud(object_segmentation_pcl::TrimCloud::Request &req, object_segmentation_pcl::TrimCloud::Response &res);

bool handle_get_bounding_cuboid(object_segmentation_pcl::GetBoundingCuboid::Request &req, object_segmentation_pcl::GetBoundingCuboid::Response &res);

bool handle_euclidean_clustering(object_segmentation_pcl::ClusterEuclidean::Request &req, object_segmentation_pcl::ClusterEuclidean::Response &res);

bool handle_segment_plane(object_segmentation_pcl::SegmentPlane::Request &req, object_segmentation_pcl::SegmentPlane::Response &res);

bool handle_segment_lccp(object_segmentation_pcl::SegmentLCCP::Request &req, object_segmentation_pcl::SegmentLCCP::Response &res);


/** Clustering pipeline
 *
 * downsamples the cloud before performing Euclidean clustering.
 * returns the largest cluster and its bounding box.
 *
 **/
bool pipeline_clustering(object_segmentation_pcl::PipelineCluster::Request &req, object_segmentation_pcl::PipelineCluster::Response &res);


 /** LCCP pipeline
 *
 * performs LCCP segmentation on an input organised cloud.
 * returns the largest central segment and its bounding box.
 *
 **/
 bool pipeline_lccp(object_segmentation_pcl::PipelineLCCP::Request &req,
                   object_segmentation_pcl::PipelineLCCP::Response &res);

#endif