#include "filtering.h"

#include <pcl/filters/voxel_grid.h> // voxel grid filter
#include <pcl/filters/radius_outlier_removal.h> // radius outlier removal
#include <pcl/common/common.h> //minmax3d
#include <pcl/filters/conditional_removal.h> // conditional removal


template <typename PointT>
void downsample(typename pcl::PointCloud<PointT>::Ptr cloud,
                typename pcl::PointCloud<PointT>::Ptr voxel_filtered_cloud,
                geometry_msgs::Vector3 leaf_size)
{
    typename pcl::PointCloud<PointT>::Ptr nan_filtered_cloud(new pcl::PointCloud<PointT>);

    // remove NaNs
    std::vector<int> nan_indices;
    pcl::removeNaNFromPointCloud(*cloud, *nan_filtered_cloud, nan_indices);
    cloud.swap(nan_filtered_cloud);

    // create a voxel grid filter
    pcl::VoxelGrid<PointT> vg;
    vg.setInputCloud(cloud);
    vg.setLeafSize(leaf_size.x, leaf_size.y, leaf_size.z);
    vg.filter(*voxel_filtered_cloud);
}



// explicit instantiations of downsample
template void downsample<pcl::PointXYZL>(
    typename pcl::PointCloud<pcl::PointXYZL>::Ptr cloud,
    typename pcl::PointCloud<pcl::PointXYZL>::Ptr voxel_filtered_cloud,
    geometry_msgs::Vector3 leaf_size);

template void downsample<pcl::PointXYZRGB>(
    typename pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud,
    typename pcl::PointCloud<pcl::PointXYZRGB>::Ptr voxel_filtered_cloud,
    geometry_msgs::Vector3 leaf_size);

template void downsample<pcl::PointXYZRGBA>(
    typename pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud,
    typename pcl::PointCloud<pcl::PointXYZRGBA>::Ptr voxel_filtered_cloud,
    geometry_msgs::Vector3 leaf_size);

template void downsample<pcl::PointXYZ>(
    typename pcl::PointCloud<pcl::PointXYZ>::Ptr cloud,
    typename pcl::PointCloud<pcl::PointXYZ>::Ptr voxel_filtered_cloud,
    geometry_msgs::Vector3 leaf_size);



void remove_outliers(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud,
                     pcl::PointCloud<pcl::PointXYZ>::Ptr outlier_filtered_cloud,
                     float radius_search,
                     int min_neighbours)
{
    pcl::PointCloud<pcl::PointXYZ>::Ptr nan_filtered_cloud(new pcl::PointCloud<pcl::PointXYZ>);

    // remove NaNs
    std::vector<int> nan_indices;
    pcl::removeNaNFromPointCloud(*cloud, *nan_filtered_cloud, nan_indices);
    cloud.swap(nan_filtered_cloud);

    // build the filter
    pcl::RadiusOutlierRemoval<pcl::PointXYZ> outrem;
    outrem.setInputCloud(cloud);
    outrem.setRadiusSearch(radius_search);
    outrem.setMinNeighborsInRadius(min_neighbours);
    outrem.setKeepOrganized(true);
    outrem.filter(*outlier_filtered_cloud);
}



template <typename ConditionT>
void add_comparison_template(ConditionT condition, pcl::PointXYZ min_point, pcl::PointXYZ max_point) {
    // add comparisons
    condition->addComparison (pcl::FieldComparison<pcl::PointXYZ>::ConstPtr (new
        pcl::FieldComparison<pcl::PointXYZ>("x", pcl::ComparisonOps::GT, min_point.x)));
    condition->addComparison (pcl::FieldComparison<pcl::PointXYZ>::ConstPtr (new
        pcl::FieldComparison<pcl::PointXYZ>("y", pcl::ComparisonOps::GT, min_point.y)));
    condition->addComparison (pcl::FieldComparison<pcl::PointXYZ>::ConstPtr (new
        pcl::FieldComparison<pcl::PointXYZ>("z", pcl::ComparisonOps::GT, min_point.z)));
    condition->addComparison (pcl::FieldComparison<pcl::PointXYZ>::ConstPtr (new
        pcl::FieldComparison<pcl::PointXYZ>("x", pcl::ComparisonOps::LT, max_point.x)));
    condition->addComparison (pcl::FieldComparison<pcl::PointXYZ>::ConstPtr (new
        pcl::FieldComparison<pcl::PointXYZ>("y", pcl::ComparisonOps::LT, max_point.y)));
    condition->addComparison (pcl::FieldComparison<pcl::PointXYZ>::ConstPtr (new
        pcl::FieldComparison<pcl::PointXYZ>("z", pcl::ComparisonOps::LT, max_point.z)));
}



void trim_cloud(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud,
                pcl::PointCloud<pcl::PointXYZ>::Ptr filtered_cloud,
                geometry_msgs::Vector3 top,
                geometry_msgs::Vector3 bottom,
                bool set_negative)
{
    pcl::PointCloud<pcl::PointXYZ>::Ptr nan_filtered_cloud(new pcl::PointCloud<pcl::PointXYZ>);

    // remove NaNs
    std::vector<int> nan_indices;
    pcl::removeNaNFromPointCloud(*cloud, *nan_filtered_cloud, nan_indices);
    cloud.swap(nan_filtered_cloud);

    // create min/max points
    pcl::PointXYZ min_point, max_point;
    pcl::PointXYZ new_min_point, new_max_point;
    pcl::getMinMax3D(*cloud, min_point, max_point);
    new_min_point.x = min_point.x + bottom.x;
    new_min_point.y = min_point.y + bottom.y;
    new_min_point.z = min_point.z + bottom.z;
    new_max_point.x = max_point.x - top.x;
    new_max_point.y = max_point.y - top.y;
    new_max_point.z = max_point.z - top.z;

    // build the filter
    pcl::ConditionalRemoval<pcl::PointXYZ> condrem;
    if (set_negative)
    {
        // flip if we want to keep only the outside
        pcl::PointXYZ temp = new_min_point;
        new_min_point = new_max_point;
        new_max_point = temp;

        // set the conditions
        pcl::ConditionOr<pcl::PointXYZ>::Ptr range_cond(new pcl::ConditionOr<pcl::PointXYZ>());
        add_comparison_template(range_cond, new_min_point, new_max_point);
        condrem.setCondition(range_cond);
    }
    else
    {
        // set the conditions
        pcl::ConditionAnd<pcl::PointXYZ>::Ptr range_cond(new pcl::ConditionAnd<pcl::PointXYZ>());
        add_comparison_template(range_cond, new_min_point, new_max_point);
        condrem.setCondition(range_cond);
    }
    condrem.setInputCloud(cloud);
    condrem.setKeepOrganized(true);
    condrem.filter(*filtered_cloud);
}