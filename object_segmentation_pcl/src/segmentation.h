#ifndef SC18J3J_SEGMENTATION_H
#define SC18J3J_SEGMENTATION_H

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

/** Plane segmentation
 *
 * use SAC to remove planes from a point cloud
 *
 * reference: https://pcl.readthedocs.io/en/latest/planar_segmentation.html
 */
void segment_plane(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud,
                   pcl::PointCloud<pcl::PointXYZ>::Ptr plane_filtered_cloud,
                   float distance_threshold);


/** LCCP segmentation
 *
 * backported from PCL 1.8.0.
 * use supervoxels and LCCP to create segmentations of 3D scenes
 *
 * reference: https://github.com/PointCloudLibrary/pcl/blob/master/examples/segmentation/example_lccp_segmentation.cpp
 */
void segment_lccp(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr& cloud,
                  pcl::PointCloud<pcl::PointXYZRGB>::Ptr& sv_coloured_cloud,
                  pcl::PointCloud<pcl::PointXYZL>::Ptr& sv_labelled_cloud,
                  pcl::PointCloud<pcl::PointXYZRGB>::Ptr& lccp_coloured_cloud,
                  pcl::PointCloud<pcl::PointXYZL>::Ptr& lccp_labelled_cloud,
                  float voxel_resolution,
                  float seed_resolution,
                  float colour_importance,
                  float spatial_importance,
                  float normal_importance,
                  bool use_single_cam_transform,
                  bool use_supervoxel_refinement,
                  float concavity_tolerance_threshold,
                  float smoothness_threshold,
                  int min_segment_size,
                  bool use_extended_convexity,
                  bool use_sanity_criterion);

#endif