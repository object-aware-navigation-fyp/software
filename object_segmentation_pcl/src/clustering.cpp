#include "clustering.h"

#include <pcl/filters/filter.h> // NaN filter
#include <pcl/kdtree/kdtree.h> // kd tree


void euclidean_clustering(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud,
                          std::vector<pcl::PointIndices>& clusters,
                          float cluster_tolerance,
                          int min_cluster_size,
                          int max_cluster_size)
{
    // remove NaNs
    cloud->is_dense = false; // removeNaNFromPointCloud does not operate on dense clouds
    pcl::PointCloud<pcl::PointXYZ>::Ptr nan_filtered_cloud(new pcl::PointCloud<pcl::PointXYZ>);
    std::vector<int> nan_indices;
    pcl::removeNaNFromPointCloud(*cloud, *nan_filtered_cloud, nan_indices);
    cloud.swap(nan_filtered_cloud);

    // create a KD Tree for cluster extraction
    pcl::search::KdTree<pcl::PointXYZ>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZ>);
    tree->setInputCloud(cloud);

    // cluster extraction
    pcl::EuclideanClusterExtraction<pcl::PointXYZ> ec;
    ec.setClusterTolerance(cluster_tolerance);
    ec.setMinClusterSize(min_cluster_size);
    ec.setMaxClusterSize(max_cluster_size);
    ec.setSearchMethod(tree);
    ec.setInputCloud(cloud);
    ec.extract(clusters);
}