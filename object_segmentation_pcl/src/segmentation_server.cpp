#include <ros/ros.h>
#include <tf/transform_listener.h>
#include <pcl_ros/transforms.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/common/common.h> // minmax3d
#include <pcl/filters/extract_indices.h> // extract indices

#include "segmentation_server.h"
#include "filtering.h"
#include "clustering.h"
#include "segmentation.h"

#include <object_segmentation_pcl/Indices.h>


// global variable - transform listener
tf::TransformListener * listener;


int main(int argc, char **argv)
{
    ros::init(argc, argv, "object_segmentation_pcl");

    // initialise the transform listener
    listener = new tf::TransformListener();
    ros::Duration(2).sleep(); // populate cache for 2s

    ros::NodeHandle n;
    ros::ServiceServer downsampling_svc = n.advertiseService("/segmentation/downsample", handle_downsample);
    ros::ServiceServer remove_outliers_svc = n.advertiseService("/segmentation/remove_outliers", handle_remove_outliers);
    ros::ServiceServer trim_cloud_svc = n.advertiseService("/segmentation/trim_cloud", handle_trim_cloud);
    ros::ServiceServer bounding_cuboid_svc = n.advertiseService("/segmentation/get_bounding_cuboid", handle_get_bounding_cuboid);
    ros::ServiceServer cluster_euclidean_svc = n.advertiseService("/segmentation/cluster_euclidean", handle_euclidean_clustering);
    ros::ServiceServer segment_plane_svc = n.advertiseService("/segmentation/segment_plane", handle_segment_plane);
    ros::ServiceServer segment_lccp_svc = n.advertiseService("/segmentation/segment_lccp", handle_segment_lccp);
    ros::ServiceServer pipeline_cluster_svc = n.advertiseService("/segmentation/pipelines/cluster", pipeline_clustering);
    ros::ServiceServer pipeline_lccp_svc = n.advertiseService("/segmentation/pipelines/lccp", pipeline_lccp);
    ros::spin();
}



template <typename PointT>
void call_downsample(object_segmentation_pcl::Downsample::Request &req, object_segmentation_pcl::Downsample::Response &res)
{
    typename pcl::PointCloud<PointT>::Ptr cloud(new pcl::PointCloud<PointT>);
    typename pcl::PointCloud<PointT>::Ptr voxel_filtered_cloud(new pcl::PointCloud<PointT>);

    pcl::fromROSMsg(req.points, *cloud);
    downsample<PointT>(cloud, voxel_filtered_cloud, req.leaf_size);
    pcl::toROSMsg(*voxel_filtered_cloud, res.points);
}



bool handle_downsample(object_segmentation_pcl::Downsample::Request &req, object_segmentation_pcl::Downsample::Response &res)
{
    pcl::PCLPointCloud2 input_pointcloud2;
    pcl_conversions::toPCL(req.points, input_pointcloud2);

    if (pcl::getFieldIndex(input_pointcloud2, "label") >= 0)
        call_downsample<pcl::PointXYZL>(req, res);
    else if (pcl::getFieldIndex(input_pointcloud2, "rgb") >= 0)
        call_downsample<pcl::PointXYZRGB>(req, res);
    else if (pcl::getFieldIndex(input_pointcloud2, "rgba") >= 0)
        call_downsample<pcl::PointXYZRGBA>(req, res);
    else
        call_downsample<pcl::PointXYZ>(req, res);

    return true;
}



bool handle_remove_outliers(object_segmentation_pcl::RemoveOutliers::Request &req, object_segmentation_pcl::RemoveOutliers::Response &res) {
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr outlier_filtered_cloud(new pcl::PointCloud<pcl::PointXYZ>);

    pcl::fromROSMsg(req.points, *cloud);
    remove_outliers(cloud, outlier_filtered_cloud, req.radius_search, req.min_neighbours);

    pcl::toROSMsg(*outlier_filtered_cloud, res.points);
    return true;
}



bool handle_trim_cloud(object_segmentation_pcl::TrimCloud::Request &req, object_segmentation_pcl::TrimCloud::Response &res) {
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr filtered_cloud(new pcl::PointCloud<pcl::PointXYZ>);

    pcl::fromROSMsg(req.points, *cloud);
    trim_cloud(cloud, filtered_cloud, req.top, req.bottom, req.set_negative);

    // set response
    pcl::toROSMsg(*filtered_cloud, res.points);
    return true;
}



bool handle_get_bounding_cuboid(object_segmentation_pcl::GetBoundingCuboid::Request &req, object_segmentation_pcl::GetBoundingCuboid::Response &res) {
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::fromROSMsg(req.points, *cloud);

    // get min max
    pcl::PointXYZ min_point, max_point;
    pcl::getMinMax3D(*cloud, min_point, max_point);

    // set response
    res.min.x = min_point.x;
    res.min.y = min_point.y;
    res.min.z = min_point.z;
    res.max.x = max_point.x;
    res.max.y = max_point.y;
    res.max.z = max_point.z;
    return true;
}



bool handle_euclidean_clustering(object_segmentation_pcl::ClusterEuclidean::Request &req, object_segmentation_pcl::ClusterEuclidean::Response &res)
{
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::fromROSMsg(req.points, *cloud);

    std::vector<pcl::PointIndices> clusters;
    euclidean_clustering(cloud, clusters, req.cluster_tolerance, req.min_cluster_size, req.max_cluster_size);

    // add all clusters to the response.
    for (std::vector<pcl::PointIndices>::const_iterator cluster = clusters.begin(); cluster != clusters.end(); ++cluster)
    {
        // create and populate indices object
        object_segmentation_pcl::Indices indices;
        for (std::vector<int>::const_iterator index = cluster->indices.begin(); index != cluster->indices.end(); ++index)
            indices.indices.push_back(*index);

        res.clusters.push_back(indices);
    }

    return true;
}



bool handle_segment_plane(object_segmentation_pcl::SegmentPlane::Request &req,
                          object_segmentation_pcl::SegmentPlane::Response &res)
{
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr plane_filtered_cloud(new pcl::PointCloud<pcl::PointXYZ>);

    // create a Point Cloud from the input and remove NaNs
    pcl::fromROSMsg(req.points, *cloud);
    segment_plane(cloud, plane_filtered_cloud, req.distance_threshold);

    pcl::toROSMsg(*plane_filtered_cloud, res.points);
    return true;
}



bool handle_segment_lccp(object_segmentation_pcl::SegmentLCCP::Request &req,
                         object_segmentation_pcl::SegmentLCCP::Response &res)
{
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZRGBA>);
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr sv_coloured_cloud(new pcl::PointCloud<pcl::PointXYZRGB>);
    pcl::PointCloud<pcl::PointXYZL>::Ptr sv_labelled_cloud(new pcl::PointCloud<pcl::PointXYZL>);
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr lccp_coloured_cloud(new pcl::PointCloud<pcl::PointXYZRGB>);
    pcl::PointCloud<pcl::PointXYZL>::Ptr lccp_labelled_cloud(new pcl::PointCloud<pcl::PointXYZL>);

    pcl::fromROSMsg(req.points, *cloud);
    segment_lccp
    (
        cloud,
        sv_coloured_cloud,
        sv_labelled_cloud,
        lccp_coloured_cloud,
        lccp_labelled_cloud,
        req.voxel_resolution,
        req.seed_resolution,
        req.colour_importance,
        req.spatial_importance,
        req.normal_importance,
        req.use_single_cam_transform,
        req.use_supervoxel_refinement,
        req.concavity_tolerance_threshold,
        req.smoothness_threshold,
        req.min_segment_size,
        req.use_extended_convexity,
        req.use_sanity_criterion
    );

    // write to response
    // only write clouds i for return_clouds[i]=true to save bandwidth
    if (req.return_clouds.size() > 0 && req.return_clouds[0])
        pcl::toROSMsg(*sv_coloured_cloud, res.sv_coloured_cloud);
    if (req.return_clouds.size() > 1 && req.return_clouds[1])
        pcl::toROSMsg(*sv_labelled_cloud, res.sv_labelled_cloud);
    if (req.return_clouds.size() > 2 && req.return_clouds[2])
        pcl::toROSMsg(*lccp_coloured_cloud, res.lccp_coloured_cloud);
    if (req.return_clouds.size() > 3 && req.return_clouds[3])
        pcl::toROSMsg(*lccp_labelled_cloud, res.lccp_labelled_cloud);
    
    return true;
}



bool pipeline_clustering(object_segmentation_pcl::PipelineCluster::Request &req, object_segmentation_pcl::PipelineCluster::Response &res)
{
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr filtered_cloud(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::fromROSMsg(req.points, *cloud);

    // downsample
    downsample<pcl::PointXYZ>(cloud, filtered_cloud, req.leaf_size);
    cloud.swap(filtered_cloud);

    // euclidean clustering
    std::vector<pcl::PointIndices> clusters;
    euclidean_clustering(cloud, clusters, req.cluster_tolerance, req.min_cluster_size, req.max_cluster_size);

    // extract the largest cluster from the cloud
    pcl::PointCloud<pcl::PointXYZ>::Ptr largest_cluster(new pcl::PointCloud<pcl::PointXYZ>);
    largest_cluster->header.stamp = cloud->header.stamp;
    largest_cluster->header.frame_id = cloud->header.frame_id;
    if (clusters.size() > 0)
    {
        pcl::PointIndices::Ptr inliers(new pcl::PointIndices());
        int max_size = 0;

        for (std::vector<pcl::PointIndices>::const_iterator cluster = clusters.begin(); cluster != clusters.end(); ++cluster)
        {
            if (cluster->indices.size() > max_size)
            {
                max_size = cluster->indices.size();
                inliers->indices = cluster->indices;
            }
        }

        // extract indices
        pcl::ExtractIndices<pcl::PointXYZ> extract;
        extract.setInputCloud(cloud);
        extract.setIndices(inliers);
        extract.setNegative(false);
        extract.filter(*largest_cluster);
    }

    // transform to target frame
    pcl::PointCloud<pcl::PointXYZ>::Ptr transformed_cloud(new pcl::PointCloud<pcl::PointXYZ>);
    pcl_ros::transformPointCloud(req.target_frame, *largest_cluster, *transformed_cloud, *listener);
    largest_cluster.swap(transformed_cloud);

    // get min max
    pcl::PointXYZ min_point, max_point;
    pcl::getMinMax3D(*largest_cluster, min_point, max_point);

    // set response
    pcl::toROSMsg(*largest_cluster, res.points);
    res.min.x = min_point.x;
    res.min.y = min_point.y;
    res.min.z = min_point.z;
    res.max.x = max_point.x;
    res.max.y = max_point.y;
    res.max.z = max_point.z;

    return true;
}


bool pipeline_lccp(object_segmentation_pcl::PipelineLCCP::Request &req,
                   object_segmentation_pcl::PipelineLCCP::Response &res)
{
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZRGBA>);
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr sv_coloured_cloud(new pcl::PointCloud<pcl::PointXYZRGB>);
    pcl::PointCloud<pcl::PointXYZL>::Ptr sv_labelled_cloud(new pcl::PointCloud<pcl::PointXYZL>);
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr lccp_coloured_cloud(new pcl::PointCloud<pcl::PointXYZRGB>);
    pcl::PointCloud<pcl::PointXYZL>::Ptr lccp_labelled_cloud(new pcl::PointCloud<pcl::PointXYZL>);

    pcl::fromROSMsg(req.points, *cloud);
    segment_lccp
    (
        cloud,
        sv_coloured_cloud,
        sv_labelled_cloud,
        lccp_coloured_cloud,
        lccp_labelled_cloud,
        req.voxel_resolution,
        req.seed_resolution,
        req.colour_importance,
        req.spatial_importance,
        req.normal_importance,
        req.use_single_cam_transform,
        req.use_supervoxel_refinement,
        req.concavity_tolerance_threshold,
        req.smoothness_threshold,
        req.min_segment_size,
        req.use_extended_convexity,
        req.use_sanity_criterion
    );

    // define centroid
    int x1 = floor(lccp_labelled_cloud->width * 0.25);
    int y1 = floor(lccp_labelled_cloud->height * 0.25);
    int x2 = ceil(lccp_labelled_cloud->width * 0.75);
    int y2 = ceil(lccp_labelled_cloud->height * 0.75);

    // count occurrences of each label in centroid
    std::map<int,int> counter;
    for (int row = y1; row < y2; ++row)
        for (int col = x1; col < x2; ++col)
            ++counter[lccp_labelled_cloud->at(row * lccp_labelled_cloud->width + col).label];

    // get the most common label != 0 (undefined label)
    int max_label = 0;
    int max_count = 0;
    for (std::map<int,int>::iterator count = counter.begin(); count != counter.end(); ++count)
    {
        if (count->first && count->second > max_count)
        {
            max_label = count->first;
            max_count = count->second;
        }
    }

    // create point cloud of the segment given by max_label
    pcl::PointCloud<pcl::PointXYZ>::Ptr largest_segment(new pcl::PointCloud<pcl::PointXYZ>);
    largest_segment->header.stamp = cloud->header.stamp;
    largest_segment->header.frame_id = cloud->header.frame_id;
    for (pcl::PointCloud<pcl::PointXYZL>::iterator point = lccp_labelled_cloud->begin(); point != lccp_labelled_cloud->end(); ++ point)
    {
        if (point->label == max_label) {
            pcl::PointXYZ point_xyz = pcl::PointXYZ();
            point_xyz.x = point->x;
            point_xyz.y = point->y;
            point_xyz.z = point->z;
            largest_segment->push_back(point_xyz);
        }
    }

    // transform to target frame
    pcl::PointCloud<pcl::PointXYZ>::Ptr transformed_cloud(new pcl::PointCloud<pcl::PointXYZ>);
    pcl_ros::transformPointCloud(req.target_frame, *largest_segment, *transformed_cloud, *listener);
    largest_segment.swap(transformed_cloud);

    // get min max
    pcl::PointXYZ min_point, max_point;
    pcl::getMinMax3D(*largest_segment, min_point, max_point);

    // write to response
    pcl::toROSMsg(*lccp_coloured_cloud, res.lccp_coloured_cloud);
    pcl::toROSMsg(*largest_segment, res.lccp_largest_centroid);
    res.min.x = min_point.x;
    res.min.y = min_point.y;
    res.min.z = min_point.z;
    res.max.x = max_point.x;
    res.max.y = max_point.y;
    res.max.z = max_point.z;

    return true;
}