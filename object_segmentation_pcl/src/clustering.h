#ifndef SC18J3J_CLUSTERING_H
#define SC18J3J_CLUSTERING_H

// data types
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include <pcl/segmentation/extract_clusters.h> // Euclidean clustering


/** Euclidean Clustering
 *
 * extract clusters using a KD Tree based on a euclidean distance threshold
 *
 * reference: https://pointclouds.org/documentation/tutorials/cluster_extraction.html
 */
void euclidean_clustering(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud,
                          std::vector<pcl::PointIndices>& clusters,
                          float cluster_tolerance,
                          int min_cluster_size,
                          int max_cluster_size);


#endif