# Object Segmentation with PCL
## Quick start
    rosrun object_segmentation_pcl segmentation_server

from within a python script:

    from object_segmentation_pcl import SegmentationClient
    sc = SegmentationClient()

## Services Available
9 ROS services are made available by the `segmentation_server` node.

Services can be called conveniently via the `SegmentationClient` python object, with brief a description of available functions below:

| Function            | Description                                                                                                                                                                                                        |
|---------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| cluster_euclidean   | Uses a PCL KDTree to form Euclidean Clusters in an input PointCloud2 where points within a distance threshold of each other are in the same cluster. Returns cluster indices.                                      |
| downsample          | Downsamples an input PointCloud2 using PCL's voxel grid filter, where points contained in each voxel of an input leaf size are approximated. Returns the downsampled cloud.                                        |
| get_bounding_cuboid | Returns PCL GetMinMax3D on the input PointCloud2.                                                                                                                                                                  |
| remove_outliers     | Any points that do not have a minimum number of neighbours within some radius are removed from the PointCloud2 using PCL's RadiusOutlierRemoval. Returns the filtered cloud.                                       |
| segment_lccp        | Uses PCL to cluster the input PointCloud2 into a supervoxel cloud before performing LCCP on the supervoxels to obtain 3D segmentations. Returns a choice of coloured and labelled supervoxel and segmented clouds. |
| segment_plane       | Segments a plane from the input PointCloud2 using PCL's RANSAC implementation for a given distance threshold. Returns the filtered cloud.                                                                           |
| trim_cloud          | Removes a user-specified portion of an input PointCloud2. Returns either the filtered cloud or the outliers from filtration.                                                                                       |
| pipeline_lccp       | NaNs are filtered and the input cloud is downsampled. LCCP clustering is performed on the the filtered cloud. Returns the largest cluster as a cloud in the map frame, along with its 3D bounding box.             |
| pipeline_cluster    | NaNs are filtered and the input cloud is downsampled. Euclidean clustering is performed on the cloud. Returns the largest cluster as a cloud in the map frame, along with its 3D bounding box.                     |

## Credits
### Point Cloud Library
https://pointclouds.org/