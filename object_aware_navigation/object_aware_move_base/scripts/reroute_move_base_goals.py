#!/usr/bin/env python
import rospy
import actionlib
from move_base_msgs.msg import MoveBaseAction, MoveBaseActionGoal
from actionlib_msgs.msg import GoalStatusArray
from threading import Lock


def status_callback(status_array):
    """
    start/stop rerouting goals depending on status
    of alternative move base server
    """
    global goal_sub, engaged
    lock.acquire()

    # check if the alternative move base server is engaged
    latest = False
    for goal_status in status_array.status_list:
        if goal_status.status == 1:
            latest = True
            break

    # if the server is currently engaged, stop rerouting goals to it
    if latest:
        goal_sub.unregister()
        engaged = 1

    # if the server is free and was previously engaged,
    # start rerouting goals to it
    elif engaged:
        goal_sub = rospy.Subscriber('/move_base/goal', MoveBaseActionGoal, reroute_goal)
        engaged = 0

    lock.release()    


def reroute_goal(goal):
    """
    reroute goals directly to move_base
    """
    move_base_client.cancel_all_goals()
    move_base_client.stop_tracking_goal()
    object_aware_client.send_goal(goal.goal)
    object_aware_client.wait_for_result()


if __name__ == '__main__':
    rospy.init_node('move_base_goal_reroute')

    # actionlib clients for both move_base servers
    move_base_client = actionlib.SimpleActionClient('/move_base', MoveBaseAction)
    move_base_client.wait_for_server()
    object_aware_client = actionlib.SimpleActionClient('/object_aware_move_base', MoveBaseAction)
    object_aware_client.wait_for_server()

    # global variable engaged and mutex to handle it
    engaged = 1
    lock = Lock()

    # alternative move_base server's status
    status_sub = rospy.Subscriber('/object_aware_move_base/status', GoalStatusArray, status_callback)

    while not rospy.is_shutdown():
        rospy.spin()