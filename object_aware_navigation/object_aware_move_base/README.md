# Object Aware Move Base
Creates an `actionlib` client to an "Object Aware Handler" when the navigation plan is about to cross a known object.

## HandleObject Action
Interface used to communicate with "Object Aware Handlers" via `actionlib`, defined in `/action/HandleObject.action`.

Input/Request:

| Name   | Type                      | Description                                  |
|--------|---------------------------|----------------------------------------------|
| marker | visualization_msgs/Marker | 3D visualisation of the object to be handled |

Output/Response:

| Name  | Type | Description                              |
|-------|------|------------------------------------------|
| error | int8 | error code of the HandleObject operation |

Custom "Object Aware Handlers" can be written in C++/python implementing the `HandleObject` action in an `actionlib` server.

Topic names of "Object Aware Handler" `actionlib` servers should match the `ns` field of the desired object's `Marker` to be called by `object_aware_move_base`.

## Quick Start
    roslaunch object_aware_move_base object_aware_move_base.launch

or alternatively, run `semantic_mapping` and relay the markers manually:

    roslaunch semantic_mapping semantic_mapping.launch
    rosrun topic_tools relay <MarkerArray topic> /move_base/global_costmap/obstacle_laser_layer/known_objects
    rosrun object_aware_move_base object_aware_move_base_server.py

## Re-routing move_base goals to object_aware_move_base

    rosrun object_aware_move_base reroute_move_base_goals.py

## Credits
### move_base
http://wiki.ros.org/move_base

### actionlib
http://wiki.ros.org/actionlib_tutorials/Tutorials/SimpleActionServer%28ExecuteCallbackMethod%29