#!/usr/bin/env python
# actionlib reference: http://wiki.ros.org/actionlib_tutorials
import rospy
import actionlib

import numpy as np
from threading import Lock
from copy import deepcopy

from move_base_msgs.msg import MoveBaseAction
from object_aware_move_base.msg import HandleObjectAction, HandleObjectGoal
from control_msgs.msg import PointHeadAction, PointHeadGoal, FollowJointTrajectoryAction, FollowJointTrajectoryGoal
from geometry_msgs.msg import Vector3
from trajectory_msgs.msg import JointTrajectoryPoint
from visualization_msgs.msg import MarkerArray
from nav_msgs.msg import Path


class ObjectAwareMoveBaseServer:
    def __init__(self, name, max_tries=3):
        self._action_name = name
        self.max_tries = 3
        self.inflation_radius = 0.27
        self.max_intersections = 2

        if rospy.has_param('/object_aware_navigation/known_object_inflation_radius'):
            self.inflation_radius = rospy.get_param('/object_aware_navigation/known_object_inflation_radius')

        if rospy.has_param('/object_aware_navigation/max_intersections'):
            self.max_intersections = rospy.get_param('/object_aware_navigation/max_intersections')

        if rospy.has_param('/object_aware_navigation/default_joint_trajectory'):
            self.default_joint_trajectory = rospy.get_param('/object_aware_navigation/default_joint_trajectory')

        # marker_array/path initialisation
        self.marker_array = MarkerArray()
        self.latest_path = Path()

        # locks for marker_array/path updates
        self.markers_lock = Lock()
        self.path_lock = Lock()

        # create move_base client
        self._move_base_client = actionlib.SimpleActionClient('/move_base', MoveBaseAction)
        self._move_base_client.wait_for_server()

        # point head client
        self._point_head_client = actionlib.SimpleActionClient('/head_controller/point_head_action', PointHeadAction)
        self._point_head_client.wait_for_server()

        # head trajectory controller
        self._head_trajectory_client = actionlib.SimpleActionClient('/head_controller/follow_joint_trajectory', FollowJointTrajectoryAction)
        self._head_trajectory_client.wait_for_server()

        # create action server
        self._as = actionlib.SimpleActionServer('/object_aware_move_base', MoveBaseAction, execute_cb=self.execute_cb, auto_start=False)
        self._as.start()

        # subscribe to semantic_map/move_base
        self.markers_sub = rospy.Subscriber('/semantic_map/markers', MarkerArray, self.set_marker_array, queue_size=1)
        self.path_sub = rospy.Subscriber('/move_base/current_plan', Path, self.set_path, queue_size=1)


    def execute_cb(self, goal):
        rospy.loginfo('Object Aware Move Base: goal at (%.2f, %.2f, %.2f)',
            goal.target_pose.pose.position.x,
            goal.target_pose.pose.position.y,
            goal.target_pose.pose.position.z)

        self._move_base_client.send_goal(goal)
        tries = 0
        head_at_default = 0

        # check for collisions at 10hz
        while not rospy.is_shutdown() and not self._move_base_client.wait_for_result(rospy.Duration(0.1)) and tries < self.max_tries:
            closest, collision = self.check_for_collision()

            # look at the closest object on our path
            if closest is not None:
                self.point_head_to_marker(closest)
                head_at_default = 0

            # back to default position using head controller.
            elif not head_at_default:
                self.send_joint_trajectory (
                    self.default_joint_trajectory['joint_names'],
                    self.default_joint_trajectory['position'],
                    self.default_joint_trajectory['time_from_start']
                )
                head_at_default = 1

            # handle collision course
            if collision:
                rospy.loginfo('Object Aware Move Base: collision detected. handling %s', closest.ns)
                self._move_base_client.cancel_all_goals()

                if self.handle_object(closest):
                    tries = 0
                    rospy.loginfo('Object Aware Move Base: handle {} successful'.format(closest.ns))
                else:
                    tries += 1
                    rospy.loginfo('Object Aware Move Base: trying again')

                # try again
                if tries < self.max_tries:
                    self._move_base_client.send_goal(goal)

        self._as.set_succeeded(True)
        rospy.loginfo('Object Aware Move Base: MoveBaseAction complete.')


    def check_for_collision(self, min_dist=2.0, max_dist=5.0):
        path = self.get_path()
        marker_array = self.get_marker_array()

        closest = None
        collision = False

        for posestamped in path.poses:
            # get distance between first and current point
            x0 = path.poses[0].pose.position.x
            y0 = path.poses[0].pose.position.y
            xk = posestamped.pose.position.x
            yk = posestamped.pose.position.y
            dist = np.linalg.norm(np.array([x0, y0]) - np.array([xk, yk]))

            # track number of inflation radii entered
            inflation_radii_entered = 0

            # check for collisions
            for marker in marker_array.markers:
                mx = marker.pose.position.x
                my = marker.pose.position.y
                mw = marker.scale.x/2.0
                mh = marker.scale.y/2.0

                mx1 = mx - mw
                my1 = my - mh
                mx2 = mx + mw
                my2 = my + mh

                mx1_inflated = mx1 - self.inflation_radius
                my1_inflated = my1 - self.inflation_radius
                mx2_inflated = mx2 + self.inflation_radius
                my2_inflated = my2 + self.inflation_radius

                if dist < max_dist and dist > self.inflation_radius:

                    # test entry into inflation radius
                    if xk > mx1_inflated and xk < mx2_inflated and yk > my1_inflated and yk < my2_inflated:
                        inflation_radii_entered += 1

                        # if within the inflation radii of two known objects
                        # or in direct collision with a known object
                        if (inflation_radii_entered >= self.max_intersections) or (xk > mx1 and xk < mx2 and yk > my1 and yk < my2):
                            closest = marker

                            # collision if within minimum distance
                            if dist < min_dist:
                                collision = True

                            break
            
            # stop checking if collision found
            if closest is not None:
                break

        return closest, collision


    def handle_object(self, marker):
        # handler topic and actionlib client
        handler_topic = '/object_aware_move_base/handlers/' + marker.ns
        handler_client = actionlib.SimpleActionClient(handler_topic, HandleObjectAction)

        # check if handler exists for this object
        if not handler_client.wait_for_server(rospy.Duration(2.0)):
            rospy.loginfo('Object Aware Move Base: actionlib server not found at %s', handler_topic)
            return 0

        # send goal and wait for result
        handler_client.send_goal(HandleObjectGoal(marker))
        handler_client.wait_for_result()

        return handler_client.get_result().error


    def point_head_to_marker(self, marker):
        ph_goal = PointHeadGoal()

        # set target point
        ph_goal.target.header.frame_id = marker.header.frame_id
        ph_goal.target.header.stamp = rospy.Time(0)
        ph_goal.target.point = marker.pose.position

        # set PointHead values
        ph_goal.pointing_axis = Vector3(1,0,0)
        ph_goal.pointing_frame = 'head_2_link'
        ph_goal.min_duration = rospy.Duration(0.2)
        ph_goal.max_velocity = 1

        self._point_head_client.send_goal(ph_goal)


    def send_joint_trajectory(self, joint_names, position, time_from_start):
        goal = FollowJointTrajectoryGoal()
        goal.trajectory.joint_names = ['head_1_joint', 'head_2_joint']

        # set first (and only) position
        goal.trajectory.points.append(JointTrajectoryPoint())
        goal.trajectory.points[0].positions = position
        goal.trajectory.points[0].time_from_start = rospy.Duration(time_from_start)

        self._head_trajectory_client.send_goal(goal)


    def set_marker_array(self, data):
        self.markers_lock.acquire()
        self.marker_array = data
        self.markers_lock.release()


    def set_path(self, data):
        self.path_lock.acquire()
        self.latest_path = data
        self.path_lock.release()


    def get_marker_array(self):
        self.markers_lock.acquire()
        marker_array = deepcopy(self.marker_array)
        self.markers_lock.release()
        return marker_array


    def get_path(self):
        self.path_lock.acquire()
        path = deepcopy(self.latest_path)
        self.path_lock.release()
        return path



if __name__ == '__main__':
    rospy.init_node('object_aware_move_base')
    server = ObjectAwareMoveBaseServer(rospy.get_name())

    while not rospy.is_shutdown():
        rospy.spin()